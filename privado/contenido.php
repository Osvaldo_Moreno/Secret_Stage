<?php

	extract ($_REQUEST);
	//Inicio la sesión 
	session_start();
	//COMPRUEBA QUE EL USUARIO ESTA AUTENTIFICADO 
	if($_SESSION["autentificado"] != "SI") { 
   	//si no existe, envio a la página de autentificacion 
   	header("Location: ../login.php"); 
   	//ademas salgo de este script 
	} else {
    //sino, calculamos el tiempo transcurrido
    $fechaGuardada = $_SESSION["ultimoAcceso"];
    $ahora = date("Y-m-d H:i:s");
    $tiempo_transcurrido = (strtotime($ahora)-strtotime($fechaGuardada));

    //comparamos el tiempo transcurrido
     if($tiempo_transcurrido >= 600) {
     //si pasaron 10 minutos o más
      session_destroy(); // destruyo la sesión
      header("Location: ../login.php"); //envío al usuario a la pag. de autenticación
      //sino, actualizo la fecha de la sesión
    }else {
    $_SESSION["ultimoAcceso"] = $ahora;
  	 }
	} 
	//incluye funciones
include '../funciones.php';
	$nombre= $_SESSION["nombre"];
	$id_usuario= $_SESSION["id_usuario"];
?>
<!DOCTYPE> 
<html>
	<head>
		<style type="text/css">
		/*agregamos el fondo */
			body{
				background: url('../imagenes/legends.jpg');
				background-size: 100%;
				background-repeat: no-repeat;
				background-attachment: fixed;
				background-color: black;
			}
		</style>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Bienvenido</title>
		<link rel="stylesheet" href="../css/estilos.css">
	</head>
	<body>
		<div id="contenedor">
			<div id="cabecera">
				<div id="logo1">
					<br/>
					<><img src="../imagenes/logo3.png">
					<br/>
				</div>
				<div id="menu">
					<ul class="horizontal">
						<font face="arial">
			  			<?php
					  		$seccion="Contenido";
					  		echo menu("./bienvenido.php", "Bienvenido", $seccion);
							echo menu("./contenido.php","Contenido",$seccion);
							echo menu("./perfil.php","Perfil",$seccion);
							echo menu("./foro.php","Foros",$seccion);
							echo menu("../index.php","Salir",$seccion);
							
					  	?>
					  	</font>
					</ul>
				</div>
			</div>
			<div >
			 <div id="stage0_0">
				<div  id="stage0_1">
					<div id="menu2">
					<ul class="vertical">
						<font face="arial" size="4">
							<b>
								<?php
					  			$seccion="Bienvenido";
					  			echo menu("./contenidos/resenas.php", "Reseñas", $seccion);
								echo menu("./contenidos/recomendaciones.php","Recomendaciones",$seccion);
								echo menu("./contenidos/noticias.php","Noticias",$seccion);
					  			?>
							</b>
					  	</font>
					</ul>
				</div>
				</div>
				<div id="temas">
					<div id="temas1">
						
					</div>
					<div id="temas2">
						
					</div>
					<div id="temas3">
						<font face="calibri" color="white" size="4">
							<h1>Lo mas nuevo:</h1>
							<p>-¿Está en desarrollo Devil May Cry 5? <a href="./contenidos/noticia/noticia1.php"><font color="white">LEER</font></a></p>
							<center>
								<img src="../imagenes/noticias1.jpg" width="500" height="300"/>
							</center>
							<p>-Reseña de: ¡TTGL! <a href="./contenidos/resena/resena4.php"><font color="white">LEER</font></a></p>
							<center>
								<img src="../imagenes/resena4.jpg" width="500" height="300"/>
							</center>
							<p>-Te recomendamos: ¡Furi! <a href="./contenidos/recomendacion/recomendacion1.php"><font color="white">LEER</font></a></p>
							<center>
								<img src="../imagenes/recomendacion.jpg" width="500" height="300"/>
							</center>
							<p>-Reseña de: ¡Soukyuu no Fafner! <a href="./contenidos/resena/resena5.php"><font color="white">LEER</font></a></p>
							<center>
								<img src="../imagenes/resena5.jpg" width="500" height="300"/>
							</center>
							<p>-Sony te dará crédito en la Playstation Store por tus trofeos <a href="./contenidos/noticia/noticia2.php"><font color="white">LEER</font></a></p>
							<center>
								<img src="../imagenes/noticia2.jpg" width="500" height="300"/>
							</center>
						</font>
					</div>
					<div id="temas4">
						
					</div>
				</div>
			 </div>
			 
			<div id="Pie">
				<div id="pie1">
					
				</div>
				<div id="pie2">
					<div id="pie2_1">
						
					</div>
					<div id="pie2_2">
						<div id="pie2_2_1">
							<font face="arial", color="white"><h3>Búscanos en nuestras redes sociales</h3></font>
						</div>
						<div id="pie2_2_2">
							<font face="arial">
								<img src="../imagenes/youtube2.png" align="center"/><a href="https://www.youtube.com/channel/UCUJH5WKAU0J2bAWG3W_ep4g", style="text-align: center"><font color="white">Canal de youtube</font></a> &emsp; &emsp; &emsp; &emsp; &emsp; &emsp;
								<img src="../imagenes/facebook.png" align="center"/><a href="https://www.facebook.com/SecretStageYoutubeChannel/", style="text-align: center"><font color="white">Página de Facebook</font></a> &emsp; &emsp; &emsp; &emsp; &emsp; &emsp;
								<img src="../imagenes/twitter.png" align="center"/><a href="https://twitter.com/Mat_Cor_Ss", style="text-align: center"><font color="white">Twitter</font></a> &emsp; &emsp; &emsp; &emsp; &emsp; &emsp;
							</font>
						</div>
					</div>
					<div id="pie2_3">
						
					</div>
				</div>
				<div id="pie3">
					
				</div>
			</div>
		</div>
	</body>
</html>