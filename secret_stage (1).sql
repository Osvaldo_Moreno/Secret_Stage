-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 05-12-2017 a las 19:32:50
-- Versión del servidor: 10.1.28-MariaDB
-- Versión de PHP: 5.6.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `secret_stage`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comentario1`
--

CREATE TABLE `comentario1` (
  `idcomentario1` int(11) NOT NULL,
  `comentarios1` longtext,
  `status` int(11) DEFAULT NULL,
  `idusuarios` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `comentario1`
--

INSERT INTO `comentario1` (`idcomentario1`, `comentarios1`, `status`, `idusuarios`) VALUES
(4, 'Y quien sabe hasta cuando saldrÃ¡ la siguiente temporada, rayos', 1, 16),
(5, 'te entiendo bro, no se sabe hasta cuando saldrÃ¡ la siguiente temporada', 1, 15),
(6, 'pues escuche que tal vez en 2018 salga la siguiente temporada, pero no es seguro', 1, 16),
(7, 'wow wow wow que? :o', 1, 15),
(8, 'es en serio?', 1, 15),
(9, 'se oye interesante el anime, despuÃ©s lo buscarÃ©', 1, 24),
(10, 'Buscalo nightblue3, es muy divertido xD', 1, 22),
(11, 'no habÃ­a oÃ­do hablar de Ã©l, tendrÃ© que buscarlo', 1, 21),
(12, 'Yo ya lo vi y no me gustÃ³ tanto como dicen', 1, 26),
(13, 'pues a mi ni me llamÃ³ mucho la atenciÃ³n que digamos', 1, 19);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comentario2`
--

CREATE TABLE `comentario2` (
  `idcomentario2` int(11) NOT NULL,
  `comentarios2` longtext,
  `status` int(11) DEFAULT NULL,
  `idusuarios` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `comentario2`
--

INSERT INTO `comentario2` (`idcomentario2`, `comentarios2`, `status`, `idusuarios`) VALUES
(2, 'wow increible reseÃ±a, aunque les faltÃ³ mencionar algunas cosas ', 1, 23),
(3, 'genial, tekken es un juego impresionante, mÃ¡s a nivel competitivo, hablen un poco acerca de los torneos que hay', 1, 25),
(4, 'Por fin, un juego que vale la pena', 1, 17),
(5, 'pero por que en PS4? jueguenlo en PC', 1, 17),
(6, 'unanse a la PC master race', 1, 17),
(7, 'yo tambiÃ©n lo juguÃ© en PS4 y si tiene esas fallas que dicen', 1, 24),
(8, 'yo ya me lo acabe completo y si me encanto cx', 1, 18),
(9, 'concuerdo contigo fede :)', 1, 15),
(10, 'yo le pondrÃ­a un 7 de calificaciÃ³n', 1, 20),
(11, 'wow tambiÃ©n tienen a eliza', 1, 22);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comentario3`
--

CREATE TABLE `comentario3` (
  `idcomentario3` int(11) NOT NULL,
  `comentarios3` longtext,
  `status` int(11) DEFAULT NULL,
  `idusuarios` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `comentario3`
--

INSERT INTO `comentario3` (`idcomentario3`, `comentarios3`, `status`, `idusuarios`) VALUES
(2, 'se ve interesante', 1, 20),
(3, 'que mal que se convierta en pay to win', 1, 25),
(4, 'es una muy mala idea que hagan eso, ya que al hacerlos pay to win, pierden la diversion', 1, 22),
(5, 'Primero las loot boxes, y ahora esto?', 1, 15),
(6, 'no me llama la atencion', 1, 17),
(7, 'wow 2007, hace 10 aÃ±os', 1, 26),
(8, 'A pesar de todo es un buen juego', 1, 16),
(9, 'mmm... free to play? lo probarÃ©', 1, 21),
(10, 'ya lo probÃ© y no me convenciÃ³', 1, 15),
(11, 'muy corta la reseÃ±a', 1, 19);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comentario4`
--

CREATE TABLE `comentario4` (
  `idcomentario4` int(11) NOT NULL,
  `comentarios4` longtext,
  `status` int(11) DEFAULT NULL,
  `idusuarios` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `comentario4`
--

INSERT INTO `comentario4` (`idcomentario4`, `comentarios4`, `status`, `idusuarios`) VALUES
(2, 'gracias por la reseÃ±a, tal vez me anime a verlo :)', 1, 21),
(3, 'wow gurren lagan', 1, 15),
(4, 'un gran anime', 1, 19),
(5, 'una joya', 1, 17),
(6, 'aÃºn no lo he acabado de ver pero es muy bueno', 1, 23),
(7, 'me lo han recomendado mucho pero aun no estoy muy seguro', 1, 24),
(8, 'todos lo recomiendan al 100%?', 1, 24),
(9, 'Â¡SÃ­!', 1, 20),
(10, 'aprovecharÃ© para verlo un dÃ­a de estos', 1, 18),
(11, 'pequeÃ±a pero de buena calidad, me gusta', 1, 26);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comentario5`
--

CREATE TABLE `comentario5` (
  `idcomentario5` int(11) NOT NULL,
  `comentarios5` longtext,
  `status` int(11) DEFAULT NULL,
  `idusuarios` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `comentario5`
--

INSERT INTO `comentario5` (`idcomentario5`, `comentarios5`, `status`, `idusuarios`) VALUES
(2, 'wow me convencieron con lo de gundam', 1, 22),
(3, 'x2', 1, 24),
(4, 'luce un poco intrigante e interesante', 1, 23),
(5, 'evangelion o gundam, ya me diÃ³ curiosidad', 1, 19),
(6, 'Â¿De quÃ© aÃ±o es el anime?', 1, 20),
(7, 'del 2004, ya tiene un poco de tiempo', 1, 18),
(8, 'alguien sabrÃ¡ donde puedo verlo?', 1, 26),
(9, 'en jkanime', 1, 25),
(10, 'o alguna otra pÃ¡gina de anime que prefieras', 1, 25),
(11, 'no me convenciÃ³ del todo', 1, 21);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comentario6`
--

CREATE TABLE `comentario6` (
  `idcomentario6` int(11) NOT NULL,
  `comentarios6` longtext,
  `status` int(11) DEFAULT NULL,
  `idusuarios` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `comentario6`
--

INSERT INTO `comentario6` (`idcomentario6`, `comentarios6`, `status`, `idusuarios`) VALUES
(2, 'genial reseÃ±a', 1, 19),
(3, 'cuantos recuerdos de este gran juego', 1, 19),
(4, 'si, yo lo juguÃ© con mi hijo', 1, 23),
(5, 'prefiero el primer resistance', 1, 22),
(6, 'no me atrajo mucho este sinceramente, y cuando lo probÃ© me decepcionÃ³', 1, 22),
(7, 'sinceramente no soy muy fan de la saga, pero aÃºn asÃ­ concuerdo en que su banda sonora es fantÃ¡stica', 1, 25),
(8, 'aunque no es suficiente para cubrir algunas deficiencias que tiene', 1, 25),
(9, 'Exactamente mi buen faker', 1, 17),
(10, 'no he tenido la oportunidad de probarlo, pero se ve que es un muy buen juego, y gracias a esta reseÃ±a ya me animÃ© mas', 1, 18),
(11, 'gracias Secret Stage :)', 1, 18);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comentario7`
--

CREATE TABLE `comentario7` (
  `idcomentario7` int(11) NOT NULL,
  `comentarios7` longtext,
  `status` int(11) DEFAULT NULL,
  `idusuarios` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `comentario7`
--

INSERT INTO `comentario7` (`idcomentario7`, `comentarios7`, `status`, `idusuarios`) VALUES
(2, 'se me hace muy similar a otro anime, pero no recuerdo cual', 1, 15),
(3, 'igual a mi', 1, 26),
(4, 'no soy el Ãºnico :)', 1, 21),
(5, 'por si alguien tiene dudas de donde puede verlo estÃ¡ en jkanime', 1, 22),
(6, 'exacto, los personajes secundarios no son nada originales, tienen toda la razÃ³n', 1, 25),
(7, 'su opening es uno de los mejores que he escuchado', 1, 20),
(8, 'y su estilo en general me recuerda a atack on titan', 1, 20),
(9, 'exactamente, tiene un estilo muy similar a shingeki no kyojin', 1, 24),
(10, 'de hecho muchos los comparan a menudo', 1, 24),
(11, 'aunque desde mi punto de visa es mejor shingeki no kyojin', 1, 24);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comentario8`
--

CREATE TABLE `comentario8` (
  `idcomentario8` int(11) NOT NULL,
  `comentarios8` longtext,
  `status` int(11) DEFAULT NULL,
  `idusuarios` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `comentario8`
--

INSERT INTO `comentario8` (`idcomentario8`, `comentarios8`, `status`, `idusuarios`) VALUES
(2, 'wow muy buen juego, lo descargarÃ© para probarlo', 1, 20),
(3, 'buena recomendaciÃ³n ', 1, 21),
(4, 'i like it', 1, 24),
(5, 'es muy buen juego', 1, 25),
(6, 'me sacÃ³ canas verdes este juego', 1, 15),
(7, 'y aÃºn no lo acabo xc', 1, 15),
(8, 'lo tengo porque me lo habÃ­an dado por PS plus y no lo recodaba xD', 1, 18),
(9, 'es hora de jugarlo ', 1, 18),
(10, 'bullet hell? acepto el reto cx', 1, 26),
(11, 'este juego si vale la pena', 1, 17);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comentario9`
--

CREATE TABLE `comentario9` (
  `idcomentario9` int(11) NOT NULL,
  `comentarios9` longtext,
  `status` int(11) DEFAULT NULL,
  `idusuarios` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `comentario9`
--

INSERT INTO `comentario9` (`idcomentario9`, `comentarios9`, `status`, `idusuarios`) VALUES
(4, 'No he jugado ningun Devil May Cry, pero si sale me animarÃ© a jugarlo c:', 1, 16),
(5, 'o por dios, por favor que se verdad :o :D', 1, 18),
(6, 'wow, wow, WOW', 1, 26),
(7, 'Dante regresa wow', 1, 24),
(8, 'pero no publiquen los que solo son rumores', 1, 17),
(9, 'un Ã­conno del gaming', 1, 23),
(10, 'esperemos que no se equivoquen', 1, 19),
(11, 'suena muy extraÃ±o :/', 1, 20),
(12, 'tambiÃ©n Nero y Vergil? :o', 1, 21),
(13, 'WOW', 1, 25);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comentario10`
--

CREATE TABLE `comentario10` (
  `idcomentario10` int(11) NOT NULL,
  `comentarios10` longtext,
  `status` int(11) DEFAULT NULL,
  `idusuarios` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `comentario10`
--

INSERT INTO `comentario10` (`idcomentario10`, `comentarios10`, `status`, `idusuarios`) VALUES
(2, 'funcionarÃ¡ tambiÃ©n con los que ganÃ© en mi PS3 o solo en PS4?', 1, 16),
(3, 'esto ya lo tenÃ­a xbox xD', 1, 19),
(4, 'por fin una recompensa para todos los gamers de sony', 1, 23),
(5, 'pero aplicarÃ¡ para los trofeos que ya tenÃ­amos?', 1, 21),
(6, 'o solo para los que consigamos despuÃ©s de que habiliten esto', 1, 21),
(7, 'que buena pregunta Juanem', 1, 25),
(8, 'una buena idea', 1, 22),
(9, 'pero como ya lo dijeron en los comentarios, no es nueva la idea', 1, 22),
(10, 'ya la tenÃ­a xbox xD', 1, 22),
(11, 'pero es bueno que ya por fin tengan algo similar los sonyers', 1, 22);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `datos_extra`
--

CREATE TABLE `datos_extra` (
  `iddatos_extra` int(11) NOT NULL,
  `foto` varchar(80) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `idusuarios` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `datos_extra`
--

INSERT INTO `datos_extra` (`iddatos_extra`, `foto`, `status`, `idusuarios`) VALUES
(10, 'Cool Text Os 172393035007843.png', 1, 15),
(11, '16864126_1385591401493179_1264568551642836750_n.jpg', 1, 16),
(12, 'NZCTq-hw_400x400.jpg', 1, 17),
(13, 'panel-69955030-image-a6dbe47e5144a8ef-320-320.png', 1, 18),
(14, 'GrFo0ArD.jpeg', 1, 19),
(15, 'thrash.jpg', 1, 20),
(16, 'juanem.jpg', 1, 21),
(17, 'gamerfriki.jpg', 1, 22),
(18, 'gusrodriguez.jpeg', 1, 23),
(19, '24580945_1679032532149063_1601957272_n.png', 1, 24),
(20, '24581078_1679032898815693_74299480_n.png', 1, 25),
(21, '24740764_1679037408815242_1857536667_n.png', 1, 26);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `foros2`
--

CREATE TABLE `foros2` (
  `idforos2` int(11) NOT NULL,
  `foro2` longtext,
  `status` int(11) DEFAULT NULL,
  `idusuarios` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `foros2`
--

INSERT INTO `foros2` (`idforos2`, `foro2`, `status`, `idusuarios`) VALUES
(10, 'ustedes que opinan, vale la pena el xbox one x?', 1, 19),
(11, 'no creo sinceramente', 1, 23),
(12, 'bueno, para los que no tengan un xbox one tal vez', 1, 23),
(13, 'mÃ¡s que un PS4 Pro sÃ­', 1, 24),
(14, 'pero como dijo gus, solo para los que aun no tienen un xbox one', 1, 24),
(15, 'o una consola de actual generaciÃ³n', 1, 24),
(16, 'mejor conviene comprar un xbox one s', 1, 16),
(17, 'cuesta menos y tiene muy buen rendimiento', 1, 16),
(18, 'yo si notÃ© varias diferencias entre el xbox one normal y el x, si tiene mucha mayor calidad', 1, 18),
(19, 'pero lo importante es lo que tu opines, ya que a muchos no les gusta y a otros si', 1, 18),
(20, 'si tienes la oportunidad mejor pruebalo por ti mismo', 1, 18);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `foros3`
--

CREATE TABLE `foros3` (
  `idforos3` int(11) NOT NULL,
  `foro3` longtext,
  `status` int(11) DEFAULT NULL,
  `idusuarios` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `foros3`
--

INSERT INTO `foros3` (`idforos3`, `foro3`, `status`, `idusuarios`) VALUES
(3, 'algÃºn free to play que recomienden amigos?', 1, 15),
(4, 'fortnite es muy bueno y para nada aburrido', 1, 20),
(5, 'Paladins', 1, 21),
(6, 'World of tanks', 1, 26),
(7, 'o world of warships', 1, 26),
(8, 'elsword', 1, 16),
(9, 'heroes of the storm', 1, 16),
(10, 'Paragon', 1, 22),
(11, 'Warframe', 1, 23),
(12, 'o league of legends', 1, 23);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `foros4`
--

CREATE TABLE `foros4` (
  `idforos4` int(11) NOT NULL,
  `foro4` longtext,
  `status` int(11) DEFAULT NULL,
  `idusuarios` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `foros4`
--

INSERT INTO `foros4` (`idforos4`, `foro4`, `status`, `idusuarios`) VALUES
(2, 'que juego me recomiendan que pruebe primero en mi nuevo xbox one x?', 1, 18),
(3, 'CALL OF DUTY World War 2', 1, 19),
(4, 'u obviamente forza motorsport /', 1, 19),
(5, '7', 1, 19),
(6, 'Gears of War 4', 1, 21),
(7, 'shadow of war', 1, 24),
(8, 'TITANFALL 2', 1, 15),
(9, 'Rise of the Tomb Rider', 1, 22),
(10, 'y creo que no hay mÃ¡s xD', 1, 22),
(11, 'por que aÃºn no optimizan todos', 1, 22);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `foros5`
--

CREATE TABLE `foros5` (
  `idforos5` int(11) NOT NULL,
  `foro5` longtext,
  `status` int(11) DEFAULT NULL,
  `idusuarios` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `foros5`
--

INSERT INTO `foros5` (`idforos5`, `foro5`, `status`, `idusuarios`) VALUES
(2, 'busquenme como YuutaAsakura', 1, 16),
(3, 'es mi Playstation ID', 1, 16),
(4, 'busquenme con el mismo nombre de usuario que tengo', 1, 20),
(5, 'en cualquier cuenta aparezco asÃ­ o como elThrash', 1, 20),
(6, 'busquenme como Blader_Os99', 1, 15),
(7, 'busquenme igual, como elgamerfriki', 1, 22),
(8, 'yo aparezco como Elgusrodriguez', 1, 23),
(9, 'de mi ya saben \"soyfedelobo\"', 1, 18),
(10, 'igual que mi nombre de usuario, Rodogonio', 1, 19),
(11, 'busquenme como Juanem87', 1, 21);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `foros6`
--

CREATE TABLE `foros6` (
  `idforos6` int(11) NOT NULL,
  `foro6` longtext,
  `status` int(11) DEFAULT NULL,
  `idusuarios` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `foros6`
--

INSERT INTO `foros6` (`idforos6`, `foro6`, `status`, `idusuarios`) VALUES
(2, 'alguien que me enseÃ±e un poco acerca de cÃ³mics?', 1, 18),
(3, 'Â¿quÃ© es lo que necesitas saber?', 1, 16),
(4, 'no se, algun cÃ³mic que recomienden para empezar?', 1, 18),
(5, 'a mi me gustaron mucho los de marvel zombies', 1, 21),
(6, 'puedes comenzar con alguno de esos', 1, 21),
(7, 'hay un cÃ³mic mexicano llamado Ekbalam', 1, 15),
(8, 'es muy bueno, deberÃ­as de buscarlo', 1, 15),
(9, 'aunque si es un poco complicado de conseguir', 1, 15),
(10, 'la serie de batman ethernal tambiÃ©n estÃ¡ muy buena', 1, 24),
(11, 'pero igualmente no se donde la puedas conseguir aÃºn', 1, 24);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `foros7`
--

CREATE TABLE `foros7` (
  `idforos7` int(11) NOT NULL,
  `foro7` longtext,
  `status` int(11) DEFAULT NULL,
  `idusuarios` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `foros7`
--

INSERT INTO `foros7` (`idforos7`, `foro7`, `status`, `idusuarios`) VALUES
(2, 'recomienden animes', 1, 22),
(3, 'que gÃ©nero en especial quieres ver?', 1, 24),
(4, 'me gusta un poco de todo', 1, 22),
(5, 'de shojo puedes buscar Fruits Basket', 1, 15),
(6, 'tambiÃ©n te recomiendo mucho el anime de Devil May Cry', 1, 15),
(7, 'No se si conozcas shingeki no Kyojin, es muy bueno', 1, 17),
(8, 'Hay anime de Devil may cry? :o', 1, 18),
(9, 'asÃ­ es, y creo que el estudio que lo hizo fue mad house', 1, 21),
(10, 'con eso ya sabes que es muy bueno', 1, 21),
(11, 'checa nuestras reseÃ±as en contenido', 1, 16),
(12, 'puede que haya alguno que te guste', 1, 16);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `foros8`
--

CREATE TABLE `foros8` (
  `idforos8` int(11) NOT NULL,
  `foro8` longtext,
  `status` int(11) DEFAULT NULL,
  `idusuarios` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `foros8`
--

INSERT INTO `foros8` (`idforos8`, `foro8`, `status`, `idusuarios`) VALUES
(2, 'visiten Level Up http://www.levelup.com', 1, 21),
(3, 'nuestro canal de youtube https://www.youtube.com/user/levelupcom', 1, 21),
(4, 'visiten mi canal https://www.youtube.com/user/TheLoquendo66', 1, 22),
(5, 'Que tal espartanos, para los que no nos conozcan este es nuestro canal https://www.youtube.com/user/SpartanGeekTV', 1, 17),
(6, '3D juegos MX https://www.youtube.com/channel/UCsNgmpJY4A-aR21Q5hDUuUQ', 1, 19),
(7, 'nuestro canal y redes estÃ¡n en el pie de pÃ¡gina cx', 1, 15),
(8, 'Buenos canales', 1, 25),
(9, 'https://www.youtube.com/user/LazaPLAYS', 1, 26),
(10, 'my channel https://www.youtube.com/user/Nightblue3', 1, 24),
(11, 'mi pequeÃ±o canal https://www.youtube.com/user/Aadryk', 1, 20);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `foros9`
--

CREATE TABLE `foros9` (
  `idforos9` int(11) NOT NULL,
  `foro9` longtext,
  `status` int(11) DEFAULT NULL,
  `idusuarios` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `foros9`
--

INSERT INTO `foros9` (`idforos9`, `foro9`, `status`, `idusuarios`) VALUES
(2, 'hola, alguna historia fuera de lo comÃºn que les haya pasado?', 1, 22),
(3, 'yo tengo algunas', 1, 25),
(4, 'platicamos por facebook', 1, 25),
(5, 'alguien aun recuerda nintendomanÃ­a?', 1, 23),
(6, 'Por favor, quien no lo recuerda :D', 1, 26),
(7, 'todos lo recordamos', 1, 19),
(8, 'por supuesto que sÃ­', 1, 21),
(9, 'no recuerdas hasta el especial de nintendomanÃ­a en Atomix?', 1, 21),
(10, 'yo no, yo fui generaciÃ³n cybernet', 1, 15),
(11, 'aunque si me platicaron de nintendomanÃ­a', 1, 15);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `foros10`
--

CREATE TABLE `foros10` (
  `idforos10` int(11) NOT NULL,
  `foro10` longtext,
  `status` int(11) DEFAULT NULL,
  `idusuarios` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `foros10`
--

INSERT INTO `foros10` (`idforos10`, `foro10`, `status`, `idusuarios`) VALUES
(2, 'que mÃ¡s les gustarÃ­a que habilitaramos en la pÃ¡gina?', 1, 15),
(3, 'que  mejoraran un poco los foros', 1, 24),
(4, 'que hablen mÃ¡s de PC', 1, 17),
(5, 'no me quejo, todo estÃ¡ bien', 1, 18),
(6, 'que se puedan utilizar emojis en los comentarios y foros', 1, 21),
(7, 'que agreguen mas opciones al sitio', 1, 26),
(8, 'a mi ya me gusta como estÃ¡ ', 1, 19),
(9, 'siento que le falta algo, pero estoy muy seguro', 1, 20),
(10, 'se siente incompleta', 1, 20),
(11, 'serÃ­a bueno que tambiÃ©n agregaran vÃ­deos', 1, 22),
(12, 'siento que ya estÃ¡ bien, pero con el paso del tiempo mejorarÃ¡', 1, 23);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `personales`
--

CREATE TABLE `personales` (
  `idpersonales` int(11) NOT NULL,
  `nombre` varchar(45) DEFAULT NULL,
  `a_paterno` varchar(45) DEFAULT NULL,
  `a_materno` varchar(45) DEFAULT NULL,
  `correo` varchar(80) DEFAULT NULL,
  `direccion` longtext,
  `status` int(11) DEFAULT NULL,
  `idusuarios` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `publicaciones`
--

CREATE TABLE `publicaciones` (
  `idpublicaciones` int(11) NOT NULL,
  `foro1` longtext,
  `status` int(11) DEFAULT NULL,
  `idusuarios` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `publicaciones`
--

INSERT INTO `publicaciones` (`idpublicaciones`, `foro1`, `status`, `idusuarios`) VALUES
(27, 'Hola, que me recomiendan, un sistema de enfriamiento Corsair h60 o un h75?', 1, 15),
(28, 'yo te recomiendo mÃ¡s el h75, sÃ³lo cuesta un poco mÃ¡s y concuerda con la relaciÃ³n costo beneficio', 1, 17),
(29, 'aunque te recomendarÃ­a que te consiguieras algunos mejores como los thermaltake', 1, 17),
(30, 'vale, muchas gracias Drak', 1, 15),
(31, 'hazle caso al gran spartan geek \"Drak\", el me armÃ³ mi genial PC cx', 1, 18),
(32, 'disculpen, que marcas de teclados econÃ³micos me recomiendan para recomendarles a algunos suscriptores?', 1, 26),
(33, 'eagle warrior o marvo', 1, 20),
(34, 'aunque tambien hay otros modelos de algunas marcas que estan baratos', 1, 20),
(35, 'cualquier duda sobre PC puedes contactarme Lazaplays', 1, 17),
(36, 'https://spartangeek.com/', 1, 17);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `idusuarios` int(11) NOT NULL,
  `usuarios` varchar(45) DEFAULT NULL,
  `pass` varchar(45) DEFAULT NULL,
  `nombre` varchar(45) DEFAULT NULL,
  `status` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`idusuarios`, `usuarios`, `pass`, `nombre`, `status`) VALUES
(15, 'Os', '202cb962ac59075b964b07152d234b70', 'Osvaldo', 1),
(16, 'YuutaAsakura', '202cb962ac59075b964b07152d234b70', 'Ares', 1),
(17, 'Drak', '202cb962ac59075b964b07152d234b70', 'Spartangeek', 1),
(18, 'soyfedelobo', '202cb962ac59075b964b07152d234b70', 'Federico', 1),
(19, 'Rodogonio', '202cb962ac59075b964b07152d234b70', 'Rodrigo Villanueva', 1),
(20, 'Thrash', '202cb962ac59075b964b07152d234b70', 'Thrash', 1),
(21, 'Juanem', '202cb962ac59075b964b07152d234b70', 'Juan', 1),
(22, 'elgamerfriki', '202cb962ac59075b964b07152d234b70', 'gamerfriki', 1),
(23, 'gusrodriguez', '202cb962ac59075b964b07152d234b70', 'Gus RodrÃ­guez', 1),
(24, 'nightblue3', '202cb962ac59075b964b07152d234b70', 'nightblue', 1),
(25, 'Faker', '202cb962ac59075b964b07152d234b70', 'faker', 1),
(26, 'Lazaplays', '202cb962ac59075b964b07152d234b70', 'Lazaplays', 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `comentario1`
--
ALTER TABLE `comentario1`
  ADD PRIMARY KEY (`idcomentario1`),
  ADD KEY `fk_comentario1_usuarios1_idx` (`idusuarios`);

--
-- Indices de la tabla `comentario2`
--
ALTER TABLE `comentario2`
  ADD PRIMARY KEY (`idcomentario2`),
  ADD KEY `fk_comentario2_usuarios1_idx` (`idusuarios`);

--
-- Indices de la tabla `comentario3`
--
ALTER TABLE `comentario3`
  ADD PRIMARY KEY (`idcomentario3`),
  ADD KEY `fk_comentario3_usuarios1_idx` (`idusuarios`);

--
-- Indices de la tabla `comentario4`
--
ALTER TABLE `comentario4`
  ADD PRIMARY KEY (`idcomentario4`),
  ADD KEY `fk_comentario4_usuarios1_idx` (`idusuarios`);

--
-- Indices de la tabla `comentario5`
--
ALTER TABLE `comentario5`
  ADD PRIMARY KEY (`idcomentario5`),
  ADD KEY `fk_comentario5_usuarios1_idx` (`idusuarios`);

--
-- Indices de la tabla `comentario6`
--
ALTER TABLE `comentario6`
  ADD PRIMARY KEY (`idcomentario6`),
  ADD KEY `fk_comentario6_usuarios1_idx` (`idusuarios`);

--
-- Indices de la tabla `comentario7`
--
ALTER TABLE `comentario7`
  ADD PRIMARY KEY (`idcomentario7`),
  ADD KEY `fk_comentario7_usuarios1_idx` (`idusuarios`);

--
-- Indices de la tabla `comentario8`
--
ALTER TABLE `comentario8`
  ADD PRIMARY KEY (`idcomentario8`),
  ADD KEY `fk_comentario8_usuarios1_idx` (`idusuarios`);

--
-- Indices de la tabla `comentario9`
--
ALTER TABLE `comentario9`
  ADD PRIMARY KEY (`idcomentario9`),
  ADD KEY `fk_comentario9_usuarios1_idx` (`idusuarios`);

--
-- Indices de la tabla `comentario10`
--
ALTER TABLE `comentario10`
  ADD PRIMARY KEY (`idcomentario10`),
  ADD KEY `fk_comentario10_usuarios1_idx` (`idusuarios`);

--
-- Indices de la tabla `datos_extra`
--
ALTER TABLE `datos_extra`
  ADD PRIMARY KEY (`iddatos_extra`),
  ADD KEY `fk_datos_extra_usuarios1_idx` (`idusuarios`);

--
-- Indices de la tabla `foros2`
--
ALTER TABLE `foros2`
  ADD PRIMARY KEY (`idforos2`),
  ADD KEY `fk_foros2_usuarios1_idx` (`idusuarios`);

--
-- Indices de la tabla `foros3`
--
ALTER TABLE `foros3`
  ADD PRIMARY KEY (`idforos3`),
  ADD KEY `fk_foros3_usuarios1_idx` (`idusuarios`);

--
-- Indices de la tabla `foros4`
--
ALTER TABLE `foros4`
  ADD PRIMARY KEY (`idforos4`),
  ADD KEY `fk_foros4_usuarios1_idx` (`idusuarios`);

--
-- Indices de la tabla `foros5`
--
ALTER TABLE `foros5`
  ADD PRIMARY KEY (`idforos5`),
  ADD KEY `fk_foros5_usuarios1_idx` (`idusuarios`);

--
-- Indices de la tabla `foros6`
--
ALTER TABLE `foros6`
  ADD PRIMARY KEY (`idforos6`),
  ADD KEY `fk_foros6_usuarios1_idx` (`idusuarios`);

--
-- Indices de la tabla `foros7`
--
ALTER TABLE `foros7`
  ADD PRIMARY KEY (`idforos7`),
  ADD KEY `fk_foros7_usuarios1_idx` (`idusuarios`);

--
-- Indices de la tabla `foros8`
--
ALTER TABLE `foros8`
  ADD PRIMARY KEY (`idforos8`),
  ADD KEY `fk_foros8_usuarios1_idx` (`idusuarios`);

--
-- Indices de la tabla `foros9`
--
ALTER TABLE `foros9`
  ADD PRIMARY KEY (`idforos9`),
  ADD KEY `fk_foros9_usuarios1_idx` (`idusuarios`);

--
-- Indices de la tabla `foros10`
--
ALTER TABLE `foros10`
  ADD PRIMARY KEY (`idforos10`),
  ADD KEY `fk_foros10_usuarios1_idx` (`idusuarios`);

--
-- Indices de la tabla `personales`
--
ALTER TABLE `personales`
  ADD PRIMARY KEY (`idpersonales`),
  ADD KEY `fk_personales_usuarios_idx` (`idusuarios`);

--
-- Indices de la tabla `publicaciones`
--
ALTER TABLE `publicaciones`
  ADD PRIMARY KEY (`idpublicaciones`),
  ADD KEY `fk_publicaciones_usuarios1_idx` (`idusuarios`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`idusuarios`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `comentario1`
--
ALTER TABLE `comentario1`
  MODIFY `idcomentario1` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT de la tabla `comentario2`
--
ALTER TABLE `comentario2`
  MODIFY `idcomentario2` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `comentario3`
--
ALTER TABLE `comentario3`
  MODIFY `idcomentario3` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `comentario4`
--
ALTER TABLE `comentario4`
  MODIFY `idcomentario4` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `comentario5`
--
ALTER TABLE `comentario5`
  MODIFY `idcomentario5` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `comentario6`
--
ALTER TABLE `comentario6`
  MODIFY `idcomentario6` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `comentario7`
--
ALTER TABLE `comentario7`
  MODIFY `idcomentario7` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `comentario8`
--
ALTER TABLE `comentario8`
  MODIFY `idcomentario8` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `comentario9`
--
ALTER TABLE `comentario9`
  MODIFY `idcomentario9` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT de la tabla `comentario10`
--
ALTER TABLE `comentario10`
  MODIFY `idcomentario10` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `datos_extra`
--
ALTER TABLE `datos_extra`
  MODIFY `iddatos_extra` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT de la tabla `foros2`
--
ALTER TABLE `foros2`
  MODIFY `idforos2` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT de la tabla `foros3`
--
ALTER TABLE `foros3`
  MODIFY `idforos3` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT de la tabla `foros4`
--
ALTER TABLE `foros4`
  MODIFY `idforos4` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `foros5`
--
ALTER TABLE `foros5`
  MODIFY `idforos5` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `foros6`
--
ALTER TABLE `foros6`
  MODIFY `idforos6` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `foros7`
--
ALTER TABLE `foros7`
  MODIFY `idforos7` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT de la tabla `foros8`
--
ALTER TABLE `foros8`
  MODIFY `idforos8` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `foros9`
--
ALTER TABLE `foros9`
  MODIFY `idforos9` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `foros10`
--
ALTER TABLE `foros10`
  MODIFY `idforos10` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT de la tabla `personales`
--
ALTER TABLE `personales`
  MODIFY `idpersonales` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `publicaciones`
--
ALTER TABLE `publicaciones`
  MODIFY `idpublicaciones` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `idusuarios` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `comentario1`
--
ALTER TABLE `comentario1`
  ADD CONSTRAINT `fk_comentario1_usuarios1` FOREIGN KEY (`idusuarios`) REFERENCES `usuarios` (`idusuarios`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `comentario2`
--
ALTER TABLE `comentario2`
  ADD CONSTRAINT `fk_comentario2_usuarios1` FOREIGN KEY (`idusuarios`) REFERENCES `usuarios` (`idusuarios`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `comentario3`
--
ALTER TABLE `comentario3`
  ADD CONSTRAINT `fk_comentario3_usuarios1` FOREIGN KEY (`idusuarios`) REFERENCES `usuarios` (`idusuarios`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `comentario4`
--
ALTER TABLE `comentario4`
  ADD CONSTRAINT `fk_comentario4_usuarios1` FOREIGN KEY (`idusuarios`) REFERENCES `usuarios` (`idusuarios`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `comentario5`
--
ALTER TABLE `comentario5`
  ADD CONSTRAINT `fk_comentario5_usuarios1` FOREIGN KEY (`idusuarios`) REFERENCES `usuarios` (`idusuarios`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `comentario6`
--
ALTER TABLE `comentario6`
  ADD CONSTRAINT `fk_comentario6_usuarios1` FOREIGN KEY (`idusuarios`) REFERENCES `usuarios` (`idusuarios`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `comentario7`
--
ALTER TABLE `comentario7`
  ADD CONSTRAINT `fk_comentario7_usuarios1` FOREIGN KEY (`idusuarios`) REFERENCES `usuarios` (`idusuarios`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `comentario8`
--
ALTER TABLE `comentario8`
  ADD CONSTRAINT `fk_comentario8_usuarios1` FOREIGN KEY (`idusuarios`) REFERENCES `usuarios` (`idusuarios`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `comentario9`
--
ALTER TABLE `comentario9`
  ADD CONSTRAINT `fk_comentario9_usuarios1` FOREIGN KEY (`idusuarios`) REFERENCES `usuarios` (`idusuarios`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `comentario10`
--
ALTER TABLE `comentario10`
  ADD CONSTRAINT `fk_comentario10_usuarios1` FOREIGN KEY (`idusuarios`) REFERENCES `usuarios` (`idusuarios`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `datos_extra`
--
ALTER TABLE `datos_extra`
  ADD CONSTRAINT `fk_datos_extra_usuarios1` FOREIGN KEY (`idusuarios`) REFERENCES `usuarios` (`idusuarios`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `foros2`
--
ALTER TABLE `foros2`
  ADD CONSTRAINT `fk_foros2_usuarios1` FOREIGN KEY (`idusuarios`) REFERENCES `usuarios` (`idusuarios`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `foros3`
--
ALTER TABLE `foros3`
  ADD CONSTRAINT `fk_foros3_usuarios1` FOREIGN KEY (`idusuarios`) REFERENCES `usuarios` (`idusuarios`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `foros4`
--
ALTER TABLE `foros4`
  ADD CONSTRAINT `fk_foros4_usuarios1` FOREIGN KEY (`idusuarios`) REFERENCES `usuarios` (`idusuarios`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `foros5`
--
ALTER TABLE `foros5`
  ADD CONSTRAINT `fk_foros5_usuarios1` FOREIGN KEY (`idusuarios`) REFERENCES `usuarios` (`idusuarios`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `foros6`
--
ALTER TABLE `foros6`
  ADD CONSTRAINT `fk_foros6_usuarios1` FOREIGN KEY (`idusuarios`) REFERENCES `usuarios` (`idusuarios`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `foros7`
--
ALTER TABLE `foros7`
  ADD CONSTRAINT `fk_foros7_usuarios1` FOREIGN KEY (`idusuarios`) REFERENCES `usuarios` (`idusuarios`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `foros8`
--
ALTER TABLE `foros8`
  ADD CONSTRAINT `fk_foros8_usuarios1` FOREIGN KEY (`idusuarios`) REFERENCES `usuarios` (`idusuarios`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `foros9`
--
ALTER TABLE `foros9`
  ADD CONSTRAINT `fk_foros9_usuarios1` FOREIGN KEY (`idusuarios`) REFERENCES `usuarios` (`idusuarios`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `foros10`
--
ALTER TABLE `foros10`
  ADD CONSTRAINT `fk_foros10_usuarios1` FOREIGN KEY (`idusuarios`) REFERENCES `usuarios` (`idusuarios`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `personales`
--
ALTER TABLE `personales`
  ADD CONSTRAINT `fk_personales_usuarios` FOREIGN KEY (`idusuarios`) REFERENCES `usuarios` (`idusuarios`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `publicaciones`
--
ALTER TABLE `publicaciones`
  ADD CONSTRAINT `fk_publicaciones_usuarios1` FOREIGN KEY (`idusuarios`) REFERENCES `usuarios` (`idusuarios`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
