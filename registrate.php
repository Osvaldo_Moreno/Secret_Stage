<?php

extract ($_REQUEST);

include './funciones.php';

session_start(); 
if(isset($_SESSION["mensaje"]))
{
$mensaje=$_SESSION["mensaje"];	
}
else {
	$mensaje="";
}

session_destroy();

?>
<!DOCTYPE> 

<html>
	<head>
		<style type="text/css">
		/*agregamos el fondo */
			body{
				background: url('./imagenes/legends.jpg');
				background-size: 100%;
				background-repeat: no-repeat;
				background-attachment: fixed;
				background-color: black;
			}
		</style>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Registrate</title>
		<link rel="stylesheet" href="./css/estilos.css">
	</head>
	<body>
		<div id="contenedor">
			<div id="cabecera">
				<div id="logo1">
					<br/>
					<><img src="./imagenes/logo3.png">
					<br/>
				</div>
				<div id="menu">
					<ul class="horizontal">
						<font face="arial">
			  			<?php
					  		$seccion="Regístrate";
					  		echo menu("./index.php", "Inicio", $seccion);
							echo menu("./que_es.php","¿Qué es Secret Stage?",$seccion);
							echo menu("./login.php","Inicia sesión",$seccion);
							echo menu("./registrate.php","Regístrate",$seccion);
					  	?>
					    </font>
					</ul>
				</div>
			</div>
			<div id="contenido6">
				<div id="registro">
					<font face="calibri", color="white", size="5"><h1>Llena todos los campos para tu registro</h1></font>
					<form action="./privado/registro.php" method="post">
					<font face="arial", color="white" size="4"><p>Tu nombre real:</p></font>
					<p><input type="text" name="nombre" value=""/></p>
					<font face="arial", color="white" size="4"><p>Usuario o apodo que quieras usar en el sitio:</p></font>
					<p><input type="text" name="usuario" value="" placeholder="Por ejemplo: Bigboss99"/>
					</p>
					<font face="arial", color="white" size="4"><p>Contraseña:</p></font>
					<p><input type="password" name="pass" value=""/></p>
					<font face="arial", color="white" size="4"><p>Confirma tu contraseña:</p></font>
					<p><input type="password" name="pass2" value=""/></p>
					<input id="boton" type="submit" value="Registrate"/>
				</form>
				<?php 
				if ($mensaje!="")
				{
					echo $mensaje;
				}
				?>
			</div>
			<div id="Pie">
				<div id="pie1">
					
				</div>
				<div id="pie2">
					<div id="pie2_1">
						
					</div>
					<div id="pie2_2">
						<div id="pie2_2_1">
							<font face="arial", color="white"><h3>Búscanos en nuestras redes sociales</h3></font>
						</div>
						<div id="pie2_2_2">
							<font face="arial">
								<img src="./imagenes/youtube2.png" align="center"/><a href="https://www.youtube.com/channel/UCUJH5WKAU0J2bAWG3W_ep4g", style="text-align: center"><font color="white">Canal de youtube</font></a> &emsp; &emsp; &emsp; &emsp; &emsp; &emsp;
								<img src="./imagenes/facebook.png" align="center"/><a href="https://www.facebook.com/SecretStageYoutubeChannel/", style="text-align: center"><font color="white">Página de Facebook</font></a> &emsp; &emsp; &emsp; &emsp; &emsp; &emsp;
								<img src="./imagenes/twitter.png" align="center"/><a href="https://twitter.com/Mat_Cor_Ss", style="text-align: center"><font color="white">Twitter</font></a> &emsp; &emsp; &emsp; &emsp; &emsp; &emsp;
							</font>
						</div>
					</div>
					<div id="pie2_3">
						
					</div>
				</div>
				<div id="pie3">
					
				</div>
			</div>
		</div>
	</body>
</html>