<?php

	extract ($_REQUEST);
	//Inicio la sesión 
	session_start();
	//COMPRUEBA QUE EL USUARIO ESTA AUTENTIFICADO 
	if($_SESSION["autentificado"] != "SI") { 
   	//si no existe, envio a la página de autentificacion 
   	header("Location: ../../../login.php"); 
   	//ademas salgo de este script 
	} else {
    //sino, calculamos el tiempo transcurrido
    $fechaGuardada = $_SESSION["ultimoAcceso"];
    $ahora = date("Y-m-d H:i:s");
    $tiempo_transcurrido = (strtotime($ahora)-strtotime($fechaGuardada));

    //comparamos el tiempo transcurrido
     if($tiempo_transcurrido >= 600) {
     //si pasaron 10 minutos o más
      session_destroy(); // destruyo la sesión
      header("Location: ../../../login.php"); //envío al usuario a la pag. de autenticación
      //sino, actualizo la fecha de la sesión
    }else {
    $_SESSION["ultimoAcceso"] = $ahora;
  	 }
	} 
	//incluye funciones
	include '../../../funciones.php';
	$nombre= $_SESSION["nombre"];
	$id_usuario= $_SESSION["id_usuario"];
	//publicacion
	if(isset($publicacion)&&$publicacion!="")
	{
		//me conecto a la base de datos
				$conecta = conectaDB();
				 if(conectaDB())
				{
				
					
					//inserto la publicacion
					$rs= mysql_query("INSERT INTO `comentario7` (`idcomentario7`, `comentarios7`, `status`, `idusuarios`) VALUES (NULL, '$publicacion', '1','$id_usuario')");
			 		if ($rs!=NULL) 
					 {
						//si es diferente de nulo inserto
						
						header("Location: ./resena7.php");  
					}
			 	
				}
	
	}
	
	
?>
<!DOCTYPE> 
<html>
	<head>
		<style type="text/css">
		/*agregamos el fondo */
			body{
				background: url('../../../imagenes/legends.jpg');
				background-size: 100%;
				background-repeat: no-repeat;
				background-attachment: fixed;
				background-color: black;
			}
		</style>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Koutetsujou no Kabaneri</title>
		<link rel="stylesheet" href="../../../css/estilos.css">
	</head>
	<body>
		<div id="contenedor">
			<div id="cabecera">
				<div id="logo1">
					<br/>
					<><img src="../../../imagenes/logo3.png">
					<br/>
				</div>
				<div id="menu">
					<ul class="horizontal">
						<font face="arial">
			  			<?php
					  		$seccion="Contenido";
					  		echo menu("../../bienvenido.php", "Bienvenido", $seccion);
							echo menu("../../contenido.php","Contenido",$seccion);
							echo menu("../../perfil.php","Perfil",$seccion);
							echo menu("../../foro.php","Foros",$seccion);
							echo menu("../../../index.php","Salir",$seccion);
							
					  	?>
					  	</font>
					</ul>
				</div>
			</div>
			<div >
			 <div id="stage0_0">
				<div  id="stage0_1">
					<div id="menu2">
					<ul class="vertical">
						<font face="arial" size="4">
							<b>
								<?php
					  			$seccion="Reseñas";
					  			echo menu("../resenas.php", "Reseñas", $seccion);
								echo menu("../recomendaciones.php","Recomendaciones",$seccion);
								echo menu("../noticias.php","Noticias",$seccion);
					  			?>
							</b>
					  	</font>
					</ul>
				</div>
				</div>
				<div id="temas">
					<div id="temas1">
						
					</div>
					<div id="temas2">
						
					</div>
					<div id="temas3">
						<font face="calibri" color="white" size="4" style="text-align: justify">
							<h1>Reseña de: ¡Koutetsujou no Kabaneri!</h1>
							<p>
								Los Kabane son criaturas agresivas que no pueden ser derrotadas a menos que su 
								corazón se perfore. Debido a esta situación, en la isla Hinomoto, la gente ha construido 
								estaciones de refugio para estar a salvo. Las personas acceden a las mercancías de la 
								estación y de transporte entre ellos con la ayuda de las Kotetsujo, unas locomotoras que 
								funcionan por vapor. Un día, una de estas locomotoras se estrella en la estación de 
								Aragane y los kabane atacan la ciudad. Así es como se produce la oportunidad perfecta 
								para que Ikoma, un joven deseoso de acabar con las criaturas, entre en acción.
							</p>
							<center>
								<img src="../../../imagenes/resena7.jpg" width="400" height="200"/>
							</center>
							<p>
								La historia avanza correctamente en los primeros capítulos, a ritmo normal, pero nunca 
								llega a emocionar por completo; ése es probablemente el mayor inconveniente que 
								encuentro. Por otro lado, hay ciertos altibajos que lo único que hacen es que uno pierda 
								interés en el show, más que nada a la mitad de la serie, cuando incluso ésta se vuelve 
								tediosa.
							</p>
							<p>
								Los personajes no aportan mucho, sobre todo los secundarios, que son demasiado 
								típicos y algunos hasta están para rellenar o para ser un elemento de lágrima fácil. Ikoma 
								y Mumei, por lo menos tienen un intento de desarrollo, a pesar de que no cierre por 
								completo. El protagonista crece a lo largo de los episodios para ser el típico salvador y la 
								chica involuciona hacia el final, para ser otro elemento dramático. 
							</p>
							<p>
								La animación, en contraste con lo anterior, está muy bien construida. La estética de 
								personajes, con un estilo medio tirando a los animes de antes, me gusta bastante y como 
								dije anteriormente, la ambientación es muy agradable a la vista. 
							</p>
							<p>
								Pese a algunas cosas, en general no desagrada. Asimismo, la música es genial y hace 
								brillar varios momentos, haciéndolos parecer mejores de lo que realmente son.
							</p>
						</font>
							<div id="comments">
								<font face="calibri" color="white" size="4">
								<?php
								echo "<h1>Comentarios</h1>";
								?>
								</font>
								<div id="comments_1">
									<form action="" method="post">
									
									<input type="text" name="publicacion" id="publicacion" value="" placeholder="Realiza tu comentario"/>
									</br>
									</br>
									<input id="boton_publica" type="submit" value="Comentar"/>
								</form>
								
										
										
								<font color="white" face="calibri" size="5">
										
								<?php
								//me conecto a la base de datos
								$conecta = conectaDB();
								 if(conectaDB())
								{
									//traigo todos los suarios
									$rs= mysql_query("SELECT * FROM comentario7 WHERE status=1 ORDER BY idcomentario7 DESC");
								
									if($rs!=NULL)
									{
										
									while ($row=mysql_fetch_array($rs)) 
												{
													$id_post=$row["idusuarios"];
													//nuevo qwery
													
													$usuario_rs= mysql_query("SELECT usuarios FROM usuarios WHERE idusuarios='$id_post' ");
													while ($row_usuario=mysql_fetch_array($usuario_rs)) 
													{
														$usuario=$row_usuario["usuarios"];
													}	
				
													$foto_rs= mysql_query("SELECT foto FROM datos_extra WHERE idusuarios='$id_post'");
													while ($row_foto=mysql_fetch_array($foto_rs))
													{
														$foto=$row_foto["foto"];
														
													}	
													
														echo '<div id="comments_2">';	
														if(isset($foto))
															{
																
																echo '<img src="../../../imagenes_usuarios/'.$foto.'" width="50" height="50" />';
																echo " $usuario :"; 
															}
															else {
																echo '<img src="../../../imagenes/sin_foto.jpg" width="50" height="50" />';
																echo " $usuario :"; 
															}
														echo '<p id="opinion">'.$row["comentarios7"]."</p>";
														
														echo "</div>";
												
												} 
									}
								}
								
									
								?>
								</font>
								</div>
						</div>
					</div>
					<div id="temas4">
						
					</div>
				</div>
			 </div>
			 
			<div id="Pie">
				<div id="pie1">
					
				</div>
				<div id="pie2">
					<div id="pie2_1">
						
					</div>
					<div id="pie2_2">
						<div id="pie2_2_1">
							<font face="arial", color="white"><h3>Búscanos en nuestras redes sociales</h3></font>
						</div>
						<div id="pie2_2_2">
							<font face="arial">
								<img src="../../../imagenes/youtube2.png" align="center"/><a href="https://www.youtube.com/channel/UCUJH5WKAU0J2bAWG3W_ep4g", style="text-align: center"><font color="white">Canal de youtube</font></a> &emsp; &emsp; &emsp; &emsp; &emsp; &emsp;
								<img src="../../../imagenes/facebook.png" align="center"/><a href="https://www.facebook.com/SecretStageYoutubeChannel/", style="text-align: center"><font color="white">Página de Facebook</font></a> &emsp; &emsp; &emsp; &emsp; &emsp; &emsp;
								<img src="../../../imagenes/twitter.png" align="center"/><a href="https://twitter.com/Mat_Cor_Ss", style="text-align: center"><font color="white">Twitter</font></a> &emsp; &emsp; &emsp; &emsp; &emsp; &emsp;
							</font>
						</div>
					</div>
					<div id="pie2_3">
						
					</div>
				</div>
				<div id="pie3">
					
				</div>
			</div>
		</div>
	</body>
</html>