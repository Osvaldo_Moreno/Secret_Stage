<?php

	extract ($_REQUEST);
	//Inicio la sesión 
	session_start();
	//COMPRUEBA QUE EL USUARIO ESTA AUTENTIFICADO 
	if($_SESSION["autentificado"] != "SI") { 
   	//si no existe, envio a la página de autentificacion 
   	header("Location: ../login.php"); 
   	//ademas salgo de este script 
	} else {
    //sino, calculamos el tiempo transcurrido
    $fechaGuardada = $_SESSION["ultimoAcceso"];
    $ahora = date("Y-m-d H:i:s");
    $tiempo_transcurrido = (strtotime($ahora)-strtotime($fechaGuardada));

    //comparamos el tiempo transcurrido
     if($tiempo_transcurrido >= 600) {
     //si pasaron 10 minutos o más
      session_destroy(); // destruyo la sesión
      header("Location: ../login.php"); //envío al usuario a la pag. de autenticación
      //sino, actualizo la fecha de la sesión
    }else {
    $_SESSION["ultimoAcceso"] = $ahora;
  	 }
	} 
	//incluye funciones
include '../funciones.php';
	$nombre= $_SESSION["nombre"];
	$id_usuario= $_SESSION["id_usuario"];
?>

<!DOCTYPE> 
<html>
	<head>
		<style type="text/css">
		/*agregamos el fondo */
			body{
				background: url('../imagenes/legends.jpg');
				background-size: 100%;
				background-repeat: no-repeat;
				background-attachment: fixed;
				background-color: black;
			}
		</style>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Foro</title>
		<link rel="stylesheet" href="../css/estilos.css">
	</head>
	<body>
		<div id="contenedor">
			<div id="cabecera">
				<div id="logo1">
					<br/>
					<><img src="../imagenes/logo3.png">
					<br/>
				</div>
				<div id="menu">
					<ul class="horizontal">
						<font face="arial">
			  			<?php
					  		$seccion="Foros";
					  		echo menu("./bienvenido.php", "Bienvenido", $seccion);
							echo menu("./contenido.php","Contenido",$seccion);
							echo menu("./perfil.php","Perfil",$seccion);
							echo menu("./foro.php","Foros",$seccion);
							echo menu("../index.php","Salir",$seccion);
							
					  	?>
					  	</font>
					</ul>
				</div>
			</div>
			<div id="foro">
				<div id="foro1">
					
				</div>
				<div id="foro2">
					
				</div>
				<div id="foro3">
					<font face="calibri" color="white" size="5" style="text-align: justify">
						<h1>¿En qué foro deseas participar?</h1>
						<p>1.- Preguntas sobre hardware (partes fisicas) de PC (Computadora) <a href="./foros/foro1.php"><font color="white">ENTRAR</font></a></p>
						<p>2.- Preguntas sobre hardware de consolas(PS4, Xbox One, Nintendo Switch, etc.) <a href="./foros/foro2.php"><font color="white">ENTRAR</font></a></p>
						<p>3.- Dudas con algún juego en PC <a href="./foros/foro3.php"><font color="white">ENTRAR</font></a></p>
						<p>4.- Dudas con algún juego en consolas <a href="./foros/foro4.php"><font color="white">ENTRAR</font></a></p>
						<p>5.- Invitaciones a jugar o para agregar amigos en PS4, Xbox One, Pc, etc. <a href="./foros/foro5.php"><font color="white">ENTRAR</font></a></p>
						<p>6.- Plática sobre cómics <a href="./foros/foro6.php"><font color="white">ENTRAR</font></a></p>
						<p>7.- Plática sobre Anime <a href="./foros/foro7.php"><font color="white">ENTRAR</font></a></p>
						<p>8.- Spam y/o publicidad de canales de Youtube, twitch, páginas de facebook, etc. <a href="./foros/foro8.php"><font color="white">ENTRAR</font></a></p>
						<p>9.- Platicar solo por platicar y conocer gente <a href="./foros/foro9.php"><font color="white">ENTRAR</font></a></p>
						<p>10.- Otros (Si crees que tu duda, sugerencia o comentario no entra en ninguna categoría puedes intentarlo aquí) <a href="./foros/foro10.php"><font color="white">ENTRAR</font></a></p>
					</font>
				</div>
				<div id="foro4">
					
				</div>
			</div>
			<div id="Pie">
				<div id="pie1">
					
				</div>
				<div id="pie2">
					<div id="pie2_1">
						
					</div>
					<div id="pie2_2">
						<div id="pie2_2_1">
							<font face="arial", color="white"><h3>Búscanos en nuestras redes sociales</h3></font>
						</div>
						<div id="pie2_2_2">
							<font face="arial">
								<img src="../imagenes/youtube2.png" align="center"/><a href="https://www.youtube.com/channel/UCUJH5WKAU0J2bAWG3W_ep4g", style="text-align: center"><font color="white">Canal de youtube</font></a> &emsp; &emsp; &emsp; &emsp; &emsp; &emsp;
								<img src="../imagenes/facebook.png" align="center"/><a href="https://www.facebook.com/SecretStageYoutubeChannel/", style="text-align: center"><font color="white">Página de Facebook</font></a> &emsp; &emsp; &emsp; &emsp; &emsp; &emsp;
								<img src="../imagenes/twitter.png" align="center"/><a href="https://twitter.com/Mat_Cor_Ss", style="text-align: center"><font color="white">Twitter</font></a> &emsp; &emsp; &emsp; &emsp; &emsp; &emsp;
							</font>
						</div>
					</div>
					<div id="pie2_3">
						
					</div>
				</div>
				<div id="pie3">
					
				</div>
			</div>
		</div>
	</body>
</html>