<?php

	extract ($_REQUEST);
	//Inicio la sesión 
	session_start();
	//COMPRUEBA QUE EL USUARIO ESTA AUTENTIFICADO 
	if($_SESSION["autentificado"] != "SI") { 
   	//si no existe, envio a la página de autentificacion 
   	header("Location: ../../../login.php"); 
   	//ademas salgo de este script 
	} else {
    //sino, calculamos el tiempo transcurrido
    $fechaGuardada = $_SESSION["ultimoAcceso"];
    $ahora = date("Y-m-d H:i:s");
    $tiempo_transcurrido = (strtotime($ahora)-strtotime($fechaGuardada));

    //comparamos el tiempo transcurrido
     if($tiempo_transcurrido >= 600) {
     //si pasaron 10 minutos o más
      session_destroy(); // destruyo la sesión
      header("Location: ../../../login.php"); //envío al usuario a la pag. de autenticación
      //sino, actualizo la fecha de la sesión
    }else {
    $_SESSION["ultimoAcceso"] = $ahora;
  	 }
	} 
	//incluye funciones
	include '../../../funciones.php';
	$nombre= $_SESSION["nombre"];
	$id_usuario= $_SESSION["id_usuario"];
	//publicacion
	if(isset($publicacion)&&$publicacion!="")
	{
		//me conecto a la base de datos
				$conecta = conectaDB();
				 if(conectaDB())
				{
				
					
					//inserto la publicacion
					$rs= mysql_query("INSERT INTO `comentario5` (`idcomentario5`, `comentarios5`, `status`, `idusuarios`) VALUES (NULL, '$publicacion', '1','$id_usuario')");
			 		if ($rs!=NULL) 
					 {
						//si es diferente de nulo inserto
						
						header("Location: ./resena5.php");  
					}
			 	
				}
	
	}
	
	
?>
<!DOCTYPE> 
<html>
	<head>
		<style type="text/css">
		/*agregamos el fondo */
			body{
				background: url('../../../imagenes/legends.jpg');
				background-size: 100%;
				background-repeat: no-repeat;
				background-attachment: fixed;
				background-color: black;
			}
		</style>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Soukyuu no Fafner</title>
		<link rel="stylesheet" href="../../../css/estilos.css">
	</head>
	<body>
		<div id="contenedor">
			<div id="cabecera">
				<div id="logo1">
					<br/>
					<><img src="../../../imagenes/logo3.png">
					<br/>
				</div>
				<div id="menu">
					<ul class="horizontal">
						<font face="arial">
			  			<?php
					  		$seccion="Contenido";
					  		echo menu("../../bienvenido.php", "Bienvenido", $seccion);
							echo menu("../../contenido.php","Contenido",$seccion);
							echo menu("../../perfil.php","Perfil",$seccion);
							echo menu("../../foro.php","Foros",$seccion);
							echo menu("../../../index.php","Salir",$seccion);
							
					  	?>
					  	</font>
					</ul>
				</div>
			</div>
			<div >
			 <div id="stage0_0">
				<div  id="stage0_1">
					<div id="menu2">
					<ul class="vertical">
						<font face="arial" size="4">
							<b>
								<?php
					  			$seccion="Reseñas";
					  			echo menu("../resenas.php", "Reseñas", $seccion);
								echo menu("../recomendaciones.php","Recomendaciones",$seccion);
								echo menu("../noticias.php","Noticias",$seccion);
					  			?>
							</b>
					  	</font>
					</ul>
				</div>
				</div>
				<div id="temas">
					<div id="temas1">
						
					</div>
					<div id="temas2">
						
					</div>
					<div id="temas3">
						<font face="calibri" color="white" size="4" style="text-align: justify">
							<h1>Reseña de Anime: ¡Soukyuu no Fafner!</h1>
							<p>
								“Tatsumiyajima es una isla situada en una zona aislada y tranquila de las Islas Japonesas. 
								No suele ocurrir gran cosa y los jóvenes de la isla van a la escuela sabiendo que tendrán 
								una vida tranquila. O eso es lo que les han enseñado… pero la verdad es diferente. El 
								destino de la humanidad está en juego y Tatsumiyajima es la última línea de defensa 
								contra un enemigo hostil e incomprensible. Y en el centro de la lucha por la continuidad 
								de la raza humana se sitúa el robot gigante Fafner, el dragón que guarda el tesoro final 
								de la humanidad.”
							</p>
							<p>
								Sin dudas un anime cargado de acción, monstruos y mechas que a más de alguno le 
								recordará a Neon Genesis Evangelion o Gundam, y es que justo combina un poco de 
								ambos; el diseño de personajes fue realizado por Hisashi Hirai que también se encargó 
								del diseño de los personajes de “Gundam” y la historia es parecida a Evangelion ya que el 
								autor de “Soukyuu no Fafner”, Nobuyoshi Habara, se inspiró en este anime, creando una 
								trama en la que unos niños son los únicos capaces de defender a la humanidad y salvarla 
								de la extinción con la ayuda de los robots gigantes llamados Fafner.
							</p>
							<center>
								<img src="../../../imagenes/resena5.jpg" width="400" height="300"/>
							</center>
							<p>
								Desde el primer capítulo vemos cuando sale a escena el Mecha principal, su diseño 
								cómo se imaginarán es parecido a los de Gundam pero un poco más estilizado. Debo 
								decir que cuando vi cómo se fusionaba el protagonista (Kazuki Manabe) con el robot 
								Fafner se me erizo la piel al imaginar el dolor cuando partes mecánicas se unen a sus 
								extremidades y no digamos cuando supe que los Fafner van acabando con la vida de sus 
								pilotos poco a poco. La historia se desarrolla en la “isla Tatsumiya”, una isla japonesa, 
								donde impera la paz hasta que un día los Festum los localizan e inicia la lucha. Para 
								terminar, cabe mencionar que aún hay detalles que aclarar, como por ejemplo de dónde 
								vienen los Festum, cómo aparecieron y por qué atacan a los humanos… solo espero que 
								los productores y el autor de este anime encuentren un espacio para aclarar estas 
								incógnitas. 
							</p>
						</font>
							<div id="comments">
								<font face="calibri" color="white" size="4">
								<?php
								echo "<h1>Comentarios</h1>";
								?>
								</font>
								<div id="comments_1">
									<form action="" method="post">
									
									<input type="text" name="publicacion" id="publicacion" value="" placeholder="Realiza tu comentario"/>
									</br>
									</br>
									<input id="boton_publica" type="submit" value="Comentar"/>
								</form>
								
										
										
								<font color="white" face="calibri" size="5">
										
								<?php
								//me conecto a la base de datos
								$conecta = conectaDB();
								 if(conectaDB())
								{
									//traigo todos los suarios
									$rs= mysql_query("SELECT * FROM comentario5 WHERE status=1 ORDER BY idcomentario5 DESC");
								
									if($rs!=NULL)
									{
										
									while ($row=mysql_fetch_array($rs)) 
												{
													$id_post=$row["idusuarios"];
													//nuevo qwery
													
													$usuario_rs= mysql_query("SELECT usuarios FROM usuarios WHERE idusuarios='$id_post' ");
													while ($row_usuario=mysql_fetch_array($usuario_rs)) 
													{
														$usuario=$row_usuario["usuarios"];
													}	
				
													$foto_rs= mysql_query("SELECT foto FROM datos_extra WHERE idusuarios='$id_post'");
													while ($row_foto=mysql_fetch_array($foto_rs))
													{
														$foto=$row_foto["foto"];
														
													}	
													
														echo '<div id="comments_2">';	
														if(isset($foto))
															{
																
																echo '<img src="../../../imagenes_usuarios/'.$foto.'" width="50" height="50" />';
																echo " $usuario :"; 
															}
															else {
																echo '<img src="../../../imagenes/sin_foto.jpg" width="50" height="50" />';
																echo " $usuario :"; 
															}
														echo '<p id="opinion">'.$row["comentarios5"]."</p>";
														
														echo "</div>";
												
												} 
									}
								}
								
									
								?>
								</font>
								</div>
						</div>
					</div>
					<div id="temas4">
						
					</div>
				</div>
			 </div>
			 
			<div id="Pie">
				<div id="pie1">
					
				</div>
				<div id="pie2">
					<div id="pie2_1">
						
					</div>
					<div id="pie2_2">
						<div id="pie2_2_1">
							<font face="arial", color="white"><h3>Búscanos en nuestras redes sociales</h3></font>
						</div>
						<div id="pie2_2_2">
							<font face="arial">
								<img src="../../../imagenes/youtube2.png" align="center"/><a href="https://www.youtube.com/channel/UCUJH5WKAU0J2bAWG3W_ep4g", style="text-align: center"><font color="white">Canal de youtube</font></a> &emsp; &emsp; &emsp; &emsp; &emsp; &emsp;
								<img src="../../../imagenes/facebook.png" align="center"/><a href="https://www.facebook.com/SecretStageYoutubeChannel/", style="text-align: center"><font color="white">Página de Facebook</font></a> &emsp; &emsp; &emsp; &emsp; &emsp; &emsp;
								<img src="../../../imagenes/twitter.png" align="center"/><a href="https://twitter.com/Mat_Cor_Ss", style="text-align: center"><font color="white">Twitter</font></a> &emsp; &emsp; &emsp; &emsp; &emsp; &emsp;
							</font>
						</div>
					</div>
					<div id="pie2_3">
						
					</div>
				</div>
				<div id="pie3">
					
				</div>
			</div>
		</div>
	</body>
</html>