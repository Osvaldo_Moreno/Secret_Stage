<?php
extract ($_REQUEST);
include './funciones.php';
?>
<!DOCTYPE> 

<html>
	<head>
		<style type="text/css">
		/*agregamos el fondo */
			body{
				background: url('./imagenes/legends.jpg');
				background-size: 100%;
				background-repeat: no-repeat;
				background-attachment: fixed;
				background-color: black;
			}
		</style>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Secret Stage</title>
		<link rel="stylesheet" href="./css/estilos.css">
	</head>
	<body>
		<div id="contenedor">
			<div id="cabecera">
				<div id="logo1">
					<br/>
					<><img src="./imagenes/logo3.png">
					<br/>
				</div>
				<div id="menu">
					<ul class="horizontal">
						<font face="arial">
			  			<?php
					  		$seccion="Inicio";
					  		echo menu("./index.php", "Inicio", $seccion);
							echo menu("./que_es.php","¿Qué es Secret Stage?",$seccion);
							echo menu("./login.php","Inicia sesión",$seccion);
							echo menu("./registrate.php","Regístrate",$seccion);
					  	?>
					  	</font>
					</ul>
				</div>
			</div>
			<div id="contenido1">
				<img src="./imagenes/logo3.jpg"/>
			</div>
			<div id="contenido2">
				<div id="contenido2_1">
					
				</div>
				<div id="contenido2_2">
					<div id="contenido2_2_1">
						
					</div>
					<div id="contenido2_2_2">
						<font face="calibri", size="6", color="white">
						</br>
						<h1 style="text-align: center">¡Bienvenido a Secret Stage!</h1>
						</font>
					</div>
					<div id="contenido2_2_3">
						
					</div>
					<div id="contenido2_2_4">
						<img src="./imagenes/tenor.gif" />
					</div>
					<div id="contenido2_2_5">
						
					</div>
				</div>
				<div id="contenido2_3">
					
				</div>
				<div id="contenido2_4">
					<div id="contenido2_4_1">
						
					</div>
					<div id="contenido2_4_2">
						<div id="contenido2_4_2_1">
							<font face="calibri", size="5", color="white">
							<h1>Avisos</h1>
							</font>
						</div>
						<div id="contenido2_4_2_2">
							<font face="calibri", color="white">
							<h1 style="text-align: center">La página funciona con regularidad</h1>
							</font>
							</div>
					</div>
					<div id="contenido2_4_3">
						
					</div>
				</div>
			</div>
			<div id="Pie">
				<div id="pie1">
					
				</div>
				<div id="pie2">
					<div id="pie2_1">
						
					</div>
					<div id="pie2_2">
						<div id="pie2_2_1">
							<font face="arial", color="white"><h3>Búscanos en nuestras redes sociales</h3></font>
						</div>
						<div id="pie2_2_2">
							<font face="arial">
								<img src="./imagenes/youtube2.png" align="center"/><a href="https://www.youtube.com/channel/UCUJH5WKAU0J2bAWG3W_ep4g", style="text-align: center"><font color="white">Canal de youtube</font></a> &emsp; &emsp; &emsp; &emsp; &emsp; &emsp;
								<img src="./imagenes/facebook.png" align="center"/><a href="https://www.facebook.com/SecretStageYoutubeChannel/", style="text-align: center"><font color="white">Página de Facebook</font></a> &emsp; &emsp; &emsp; &emsp; &emsp; &emsp;
								<img src="./imagenes/twitter.png" align="center"/><a href="https://twitter.com/Mat_Cor_Ss", style="text-align: center"><font color="white">Twitter</font></a> &emsp; &emsp; &emsp; &emsp; &emsp; &emsp;
							</font>
						</div>
					</div>
					<div id="pie2_3">
						
					</div>
				</div>
				<div id="pie3">
					
				</div>
			</div>
		</div>
	</body>
</html>