<?php

	extract ($_REQUEST);
	//Inicio la sesión 
	session_start();
	//COMPRUEBA QUE EL USUARIO ESTA AUTENTIFICADO 
	if($_SESSION["autentificado"] != "SI") { 
   	//si no existe, envio a la página de autentificacion 
   	header("Location: ../login.php"); 
   	//ademas salgo de este script 
	} else {
    //sino, calculamos el tiempo transcurrido
    $fechaGuardada = $_SESSION["ultimoAcceso"];
    $ahora = date("Y-m-d H:i:s");
    $tiempo_transcurrido = (strtotime($ahora)-strtotime($fechaGuardada));

    //comparamos el tiempo transcurrido
     if($tiempo_transcurrido >= 600) {
     //si pasaron 10 minutos o más
      session_destroy(); // destruyo la sesión
      header("Location: ../login.php"); //envío al usuario a la pag. de autenticación
      //sino, actualizo la fecha de la sesión
    }else {
    $_SESSION["ultimoAcceso"] = $ahora;
  	 }
	} 
	//incluye funciones
include '../funciones.php';
	$nombre= $_SESSION["nombre"];
	//id del usuario
	$id_usuario= $_SESSION["id_usuario"];
	//mensaje
	if(isset($_SESSION["mensaje"]))
	{
	$mensaje=$_SESSION["mensaje"];
	//paso a vacio	
	$_SESSION["mensaje"]= "";
	}
	else {
		$mensaje="";
	}
?>

<!DOCTYPE> 
<html>
	<head>
		<style type="text/css">
		/*agregamos el fondo */
			body{
				background: url('../imagenes/legends.jpg');
				background-size: 100%;
				background-repeat: no-repeat;
				background-attachment: fixed;
				background-color: black;
			}
		</style>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Perfil de Usuario</title>
		<link rel="stylesheet" href="../css/estilos.css">
	</head>
	<body>
		<div id="contenedor">
			<div id="cabecera">
				<div id="logo1">
					<br/>
					<><img src="../imagenes/logo3.png">
					<br/>
				</div>
				<div id="menu">
					<ul class="horizontal">
						<font face="arial">
			  			<?php
					  		$seccion="Perfil";
					  		echo menu("./bienvenido.php", "Bienvenido", $seccion);
							echo menu("./contenido.php","Contenido",$seccion);
							echo menu("./perfil.php","Perfil",$seccion);
							echo menu("./foro.php","Foros",$seccion);
							echo menu("../index.php","Salir",$seccion);
							
					  	?>
					  	</font>
					</ul>
				</div>
			</div>
			<div id="perfil">
				<div id="perfil_1">
					
				</div>
				<div id="perfil_2">
					
				</div>
				<div id="perfil_3">
					<div id="perfil_3_1">
						<font face="calibri" size="6" color="white">
						<h3>Estos son todos tus datos, aquí puedes modificarlos</h3>
						</font>
							<?php 
							if ($mensaje!="")
							{
								echo $mensaje;
							}
							?>
						<font face="calibri" size="5" color="white">
						<p>
							Si deseas cambiar tus datos que ingresaste al registrarte (Nombre, usuario y contraseña) haz click en este botón:
							
						</p>
						</font>
						<?php
						//me conecto a la base de datos
						$conecta = conectaDB();
						 if(conectaDB())
						{
							//traigo todos los suarios
							$rs= mysql_query("SELECT * FROM usuarios WHERE idusuarios = '$id_usuario'");
						
							if($rs!=NULL)
							{
								
							while ($row=mysql_fetch_array($rs)) 
										{
										echo '<center><button style="width: 200px; height: 70px;"><font size="5")><a href="./cambiar_perfil.php?id='.$row["idusuarios"].'">Modificar</a></font></button></center>';
										
										} 
							}
						}
						else {
	
						}
							
						?>
					</div>
					<div id="perfil_3_2">
						<font face="calibri" color="white" size="5">
							<p>Si deseas agregar una foto de perfil súbela en el siguiente apartado:</p>
							<font face="calibri" color="white" size="4">
								<p>*Si ya cuentas con una foto, presiona en cambiar para eliminar esa imágen y después subes tu nueva foto</p>
							</font>
						</font>
						<?php
							//valido que haya seleccionado foto	
							if(isset($_FILES['foto']))
							{
								//valido que subio al servidor
								if($_FILES['foto']['size']>0)
								{
									//indoco directorio
									$dir_subida = '../imagenes_usuarios/';
									//cargo el archivo
									$fichero_subido = $dir_subida . basename($_FILES['foto']['name']);
									//valido si se subio
									if (move_uploaded_file($_FILES['foto']['tmp_name'], $fichero_subido)) {
										//asigo el valor de la foto
										$foto=$_FILES['foto']['name'];
									} 
									else {
										$foto="";
									}
								}
							}
								
							//conectarnos a la base
							$conecta = conectaDB();
							if(conectaDB())
							{
								//guardamos informacion
								if(isset($bandera)&&$bandera==1)
								{
									$foto=(isset($foto)) ?  $foto: "";
									$valida= mysql_query("UPDATE datos_extra SET foto='$foto' WHERE idusuarios=$id_usuario");
								 		
									 //validsmo que haya insertado
									if($valida != null)
									{
										//si es diferente de nulo inserto
										
										$_SESSION["mensaje"]= '<div id="exitomjs"><font color="#5EFF00", face="arial", size="6"><p>Se actualizaron tus datos</p></font></div>'; 
											 
									}
												 
								}
								elseif(isset($bandera)&&$bandera==0) {
									$foto=(isset($foto)) ?  $foto: "";
									$rs= mysql_query("INSERT INTO `datos_extra` (`iddatos_extra`,`foto`, `status`,`idusuarios`) VALUES (NULL, '$foto', '1','$id_usuario')");
								 	//echo "INSERT INTO `info_adicional` (`idinfo_adicional`, `residencia`, `edad`, `sexo`, `status`,`idusarios`) VALUES (NULL, '$residencia', '$edad', '$sexo', '1','$id_usuario')";
									//validsmo que haya insertado
									if ($rs != null) 
									{
										//si es diferente de nulo inserto
										
										$_SESSION["mensaje"]= '<div id="exitomjs"><font color="#5EFF00", face="arial", size="6"><p>Se actualizaron tus datos</p></font></div>'; 
											
									}
								}
								else {
										$foto=null;
								}
						  		
								  	
										
								// echo "SELECT * FROM usario WHERE usuario = '$usuario' AND pass = '$pass'";
								$rs= mysql_query("SELECT * FROM datos_extra WHERE idusuarios = '$id_usuario'");
								//echo $id;
								if(mysql_num_rows ($rs) == 1)
								{
									//sie entra es que tiene datos
									$bandera=1;
									while ($row=mysql_fetch_array($rs)) 
									{ 
										$foto=$row["foto"];
									} 
								}
								else {
									$bandera=0;
								}
							
							}
									//if corto
								
									//echo (condicion) ? "verdfadera" : "falsa";
						?>	
								
						<div id="login2">
							<div id="foto">
								<?php
									if($foto!=NULL)
									{
										echo '<img src="../imagenes_usuarios/'.$foto.'" width="380" height="380" />';
									}
									else {
										echo '<img src="../imagenes/sin_foto.jpg" width="380" height="380" />';
									}
								?>
									
							</div>
							<form action="#" method="post" enctype="multipart/form-data">
							<font face="calibri" color="white"><p>Foto:</p></font>
							<p><input type="file" name="foto" />
							<center><input id="boton" type="submit" value="<?php echo ($bandera==1) ?  "Cambiar": "Subir";?>"/></center>
							<input type="hidden" name="bandera" value="<?php echo ($bandera); ?>" />
							</form>

							
						
						</div>
					</div>
				</div>
				<div id="perfil_4">
					
				</div>
			</div>
			<div id="Pie">
				<div id="pie1">
					
				</div>
				<div id="pie2">
					<div id="pie2_1">
						
					</div>
					<div id="pie2_2">
						<div id="pie2_2_1">
							<font face="arial", color="white"><h3>Búscanos en nuestras redes sociales</h3></font>
						</div>
						<div id="pie2_2_2">
							<font face="arial">
								<img src="../imagenes/youtube2.png" align="center"/><a href="https://www.youtube.com/channel/UCUJH5WKAU0J2bAWG3W_ep4g", style="text-align: center"><font color="white">Canal de youtube</font></a> &emsp; &emsp; &emsp; &emsp; &emsp; &emsp;
								<img src="../imagenes/facebook.png" align="center"/><a href="https://www.facebook.com/SecretStageYoutubeChannel/", style="text-align: center"><font color="white">Página de Facebook</font></a> &emsp; &emsp; &emsp; &emsp; &emsp; &emsp;
								<img src="../imagenes/twitter.png" align="center"/><a href="https://twitter.com/Mat_Cor_Ss", style="text-align: center"><font color="white">Twitter</font></a> &emsp; &emsp; &emsp; &emsp; &emsp; &emsp;
							</font>
						</div>
					</div>
					<div id="pie2_3">
						
					</div>
				</div>
				<div id="pie3">
					
				</div>
			</div>
		</div>
	</body>
</html>