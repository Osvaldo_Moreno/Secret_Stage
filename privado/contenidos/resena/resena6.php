<?php

	extract ($_REQUEST);
	//Inicio la sesión 
	session_start();
	//COMPRUEBA QUE EL USUARIO ESTA AUTENTIFICADO 
	if($_SESSION["autentificado"] != "SI") { 
   	//si no existe, envio a la página de autentificacion 
   	header("Location: ../../../login.php"); 
   	//ademas salgo de este script 
	} else {
    //sino, calculamos el tiempo transcurrido
    $fechaGuardada = $_SESSION["ultimoAcceso"];
    $ahora = date("Y-m-d H:i:s");
    $tiempo_transcurrido = (strtotime($ahora)-strtotime($fechaGuardada));

    //comparamos el tiempo transcurrido
     if($tiempo_transcurrido >= 600) {
     //si pasaron 10 minutos o más
      session_destroy(); // destruyo la sesión
      header("Location: ../../../login.php"); //envío al usuario a la pag. de autenticación
      //sino, actualizo la fecha de la sesión
    }else {
    $_SESSION["ultimoAcceso"] = $ahora;
  	 }
	} 
	//incluye funciones
	include '../../../funciones.php';
	$nombre= $_SESSION["nombre"];
	$id_usuario= $_SESSION["id_usuario"];
	//publicacion
	if(isset($publicacion)&&$publicacion!="")
	{
		//me conecto a la base de datos
				$conecta = conectaDB();
				 if(conectaDB())
				{
				
					
					//inserto la publicacion
					$rs= mysql_query("INSERT INTO `comentario6` (`idcomentario6`, `comentarios6`, `status`, `idusuarios`) VALUES (NULL, '$publicacion', '1','$id_usuario')");
			 		if ($rs!=NULL) 
					 {
						//si es diferente de nulo inserto
						
						header("Location: ./resena6.php");  
					}
			 	
				}
	
	}
	
	
?>
<!DOCTYPE> 
<html>
	<head>
		<style type="text/css">
		/*agregamos el fondo */
			body{
				background: url('../../../imagenes/legends.jpg');
				background-size: 100%;
				background-repeat: no-repeat;
				background-attachment: fixed;
				background-color: black;
			}
		</style>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Resistance 2</title>
		<link rel="stylesheet" href="../../../css/estilos.css">
	</head>
	<body>
		<div id="contenedor">
			<div id="cabecera">
				<div id="logo1">
					<br/>
					<><img src="../../../imagenes/logo3.png">
					<br/>
				</div>
				<div id="menu">
					<ul class="horizontal">
						<font face="arial">
			  			<?php
					  		$seccion="Contenido";
					  		echo menu("../../bienvenido.php", "Bienvenido", $seccion);
							echo menu("../../contenido.php","Contenido",$seccion);
							echo menu("../../perfil.php","Perfil",$seccion);
							echo menu("../../foro.php","Foros",$seccion);
							echo menu("../../../index.php","Salir",$seccion);
							
					  	?>
					  	</font>
					</ul>
				</div>
			</div>
			<div >
			 <div id="stage0_0">
				<div  id="stage0_1">
					<div id="menu2">
					<ul class="vertical">
						<font face="arial" size="4">
							<b>
								<?php
					  			$seccion="Reseñas";
					  			echo menu("../resenas.php", "Reseñas", $seccion);
								echo menu("../recomendaciones.php","Recomendaciones",$seccion);
								echo menu("../noticias.php","Noticias",$seccion);
					  			?>
							</b>
					  	</font>
					</ul>
				</div>
				</div>
				<div id="temas">
					<div id="temas1">
						
					</div>
					<div id="temas2">
						
					</div>
					<div id="temas3">
						<font face="calibri" color="white" size="4" style="text-align: justify">
							<h1>Reseña de: ¡Resistance 2!</h1>
							<p>
								Echando un rápido vistazo atrás recordaremos brevemente que el Resistance original se 
								desarrollaba en un mundo paralelo a mediados del siglo pasado, en el que los diferentes 
								bandos que se enfrentaron durante la Segunda Guerra Mundial dejaban de lado sus 
								diferencias con la intención de superar una amenaza mayor, la de las peligrosas 
								Chimeras. ¿De dónde salen estas criaturas? El primer título no daba demasiadas 
								explicaciones, dejando las conclusiones en el aire. Lo poco que sabemos de esta dañina y 
								monstruosa raza es que son alienígenas, y que su avanzada tecnología se extiende cual 
								infección.
							</p>
							<p>
								Resistance 2 comienza prácticamente donde acabó Fall of Man, con el soldado Hale en 
								Estados Unidos, último bastión de la humanidad. Lamentablemente esta aparentemente 
								inexpugnable fortaleza que es el continente norteamericano sufre un ataque simultáneo 
								a sus ciudades más importantes por parte de este salvaje enemigo.
							</p>
							<center>
								<img src="../../../imagenes/resena6_1.jpg" width="400" height="200"/>
							</center>
							<p>
								Así en la campaña del título visitaremos una gran variedad de escenarios urbanos, 
								rurales e industriales; que multiplican la valía del título al ampliar la diversidad de la 
								encorsetada primera parte.
							</p>
							<p>
								Por otra parte, el guion de este Resistance 2 no es demasiado interesante y sirve, como 
								es tradicional en el género, como mera forma de enlazar los diferentes capítulos y 
								lugares que conforman los siete capítulos del modo individual que pueden ser superados 
								en cerca de 10 horas. Se introducen algunos elementos extra como la agencia 
								gubernamental secreta de supersoldados llamados “Los Centinelas” que son la última 
								esperanza del ser humano, y descubrimos más sobre la amenaza Chimera de lo que 
								conocimos en la exigua primera parte.
							</p>
							<p>
								Se responden a muchas incógnitas que planteó el inicio de la saga, pero también se 
								formulan nuevas que quedan sin contestación. La presencia de Rachael como narradora 
								del original se fulmina de un plumazo en la secuela, y en esta ocasión nos enfrentamos a 
								una forma de contarnos la historia más directa, aunque, posiblemente, algo más 
								prosaica. Falta algo de empaque, por lo tanto, al argumento de Resistance 2, y de no ser 
								un título tan redondo en prácticamente el resto de sus facetas no insistiríamos tanto en 
								ello, puesto que las deficiencias de guion vienen a ser una triste constante en los juegos 
								de acción.
							</p>
							<center>
								<img src="../../../imagenes/resena6.jpg" width="400" height="200"/>
							</center>
							<p>
								El sonido es otro apartado que brilla. La banda sonora es fantástica, con unos temas 
								musicales épicos que mezclan los toques militares con las melodías más evocadoras. Los 
								efectos de sonido son tan potentes como pudiéramos esperar de un título bélico de 
								estas características; aunque el doblaje a nuestro idioma, muy correcto en líneas 
								generales, acusa serios problemas con la sincronización labial.
							</p>
						</font>
							<div id="comments">
								<font face="calibri" color="white" size="4">
								<?php
								echo "<h1>Comentarios</h1>";
								?>
								</font>
								<div id="comments_1">
									<form action="" method="post">
									
									<input type="text" name="publicacion" id="publicacion" value="" placeholder="Realiza tu comentario"/>
									</br>
									</br>
									<input id="boton_publica" type="submit" value="Comentar"/>
								</form>
								
										
										
								<font color="white" face="calibri" size="5">
										
								<?php
								//me conecto a la base de datos
								$conecta = conectaDB();
								 if(conectaDB())
								{
									//traigo todos los suarios
									$rs= mysql_query("SELECT * FROM comentario6 WHERE status=1 ORDER BY idcomentario6 DESC");
								
									if($rs!=NULL)
									{
										
									while ($row=mysql_fetch_array($rs)) 
												{
													$id_post=$row["idusuarios"];
													//nuevo qwery
													
													$usuario_rs= mysql_query("SELECT usuarios FROM usuarios WHERE idusuarios='$id_post' ");
													while ($row_usuario=mysql_fetch_array($usuario_rs)) 
													{
														$usuario=$row_usuario["usuarios"];
													}	
				
													$foto_rs= mysql_query("SELECT foto FROM datos_extra WHERE idusuarios='$id_post'");
													while ($row_foto=mysql_fetch_array($foto_rs))
													{
														$foto=$row_foto["foto"];
														
													}	
													
														echo '<div id="comments_2">';	
														if(isset($foto))
															{
																
																echo '<img src="../../../imagenes_usuarios/'.$foto.'" width="50" height="50" />';
																echo " $usuario :"; 
															}
															else {
																echo '<img src="../../../imagenes/sin_foto.jpg" width="50" height="50" />';
																echo " $usuario :"; 
															}
														echo '<p id="opinion">'.$row["comentarios6"]."</p>";
														
														echo "</div>";
												
												} 
									}
								}
								
									
								?>
								</font>
								</div>
						</div>
					</div>
					<div id="temas4">
						
					</div>
				</div>
			 </div>
			 
			<div id="Pie">
				<div id="pie1">
					
				</div>
				<div id="pie2">
					<div id="pie2_1">
						
					</div>
					<div id="pie2_2">
						<div id="pie2_2_1">
							<font face="arial", color="white"><h3>Búscanos en nuestras redes sociales</h3></font>
						</div>
						<div id="pie2_2_2">
							<font face="arial">
								<img src="../../../imagenes/youtube2.png" align="center"/><a href="https://www.youtube.com/channel/UCUJH5WKAU0J2bAWG3W_ep4g", style="text-align: center"><font color="white">Canal de youtube</font></a> &emsp; &emsp; &emsp; &emsp; &emsp; &emsp;
								<img src="../../../imagenes/facebook.png" align="center"/><a href="https://www.facebook.com/SecretStageYoutubeChannel/", style="text-align: center"><font color="white">Página de Facebook</font></a> &emsp; &emsp; &emsp; &emsp; &emsp; &emsp;
								<img src="../../../imagenes/twitter.png" align="center"/><a href="https://twitter.com/Mat_Cor_Ss", style="text-align: center"><font color="white">Twitter</font></a> &emsp; &emsp; &emsp; &emsp; &emsp; &emsp;
							</font>
						</div>
					</div>
					<div id="pie2_3">
						
					</div>
				</div>
				<div id="pie3">
					
				</div>
			</div>
		</div>
	</body>
</html>