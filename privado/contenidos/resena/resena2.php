<?php

	extract ($_REQUEST);
	//Inicio la sesión 
	session_start();
	//COMPRUEBA QUE EL USUARIO ESTA AUTENTIFICADO 
	if($_SESSION["autentificado"] != "SI") { 
   	//si no existe, envio a la página de autentificacion 
   	header("Location: ../../../login.php"); 
   	//ademas salgo de este script 
	} else {
    //sino, calculamos el tiempo transcurrido
    $fechaGuardada = $_SESSION["ultimoAcceso"];
    $ahora = date("Y-m-d H:i:s");
    $tiempo_transcurrido = (strtotime($ahora)-strtotime($fechaGuardada));

    //comparamos el tiempo transcurrido
     if($tiempo_transcurrido >= 600) {
     //si pasaron 10 minutos o más
      session_destroy(); // destruyo la sesión
      header("Location: ../../../login.php"); //envío al usuario a la pag. de autenticación
      //sino, actualizo la fecha de la sesión
    }else {
    $_SESSION["ultimoAcceso"] = $ahora;
  	 }
	} 
	//incluye funciones
	include '../../../funciones.php';
	$nombre= $_SESSION["nombre"];
	$id_usuario= $_SESSION["id_usuario"];
	//publicacion
	if(isset($publicacion)&&$publicacion!="")
	{
		//me conecto a la base de datos
				$conecta = conectaDB();
				 if(conectaDB())
				{
				
					
					//inserto la publicacion
					$rs= mysql_query("INSERT INTO `comentario2` (`idcomentario2`, `comentarios2`, `status`, `idusuarios`) VALUES (NULL, '$publicacion', '1','$id_usuario')");
			 		if ($rs!=NULL) 
					 {
						//si es diferente de nulo inserto
						
						header("Location: ./resena2.php");  
					}
			 	
				}
	
	}
	
	
?>
<!DOCTYPE> 
<html>
	<head>
		<style type="text/css">
		/*agregamos el fondo */
			body{
				background: url('../../../imagenes/legends.jpg');
				background-size: 100%;
				background-repeat: no-repeat;
				background-attachment: fixed;
				background-color: black;
			}
		</style>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>TEKKEN 7</title>
		<link rel="stylesheet" href="../../../css/estilos.css">
	</head>
	<body>
		<div id="contenedor">
			<div id="cabecera">
				<div id="logo1">
					<br/>
					<><img src="../../../imagenes/logo3.png">
					<br/>
				</div>
				<div id="menu">
					<ul class="horizontal">
						<font face="arial">
			  			<?php
					  		$seccion="Contenido";
					  		echo menu("../../bienvenido.php", "Bienvenido", $seccion);
							echo menu("../../contenido.php","Contenido",$seccion);
							echo menu("../../perfil.php","Perfil",$seccion);
							echo menu("../../foro.php","Foros",$seccion);
							echo menu("../../../index.php","Salir",$seccion);
							
					  	?>
					  	</font>
					</ul>
				</div>
			</div>
			<div >
			 <div id="stage0_0">
				<div  id="stage0_1">
					<div id="menu2">
					<ul class="vertical">
						<font face="arial" size="4">
							<b>
								<?php
					  			$seccion="Reseñas";
					  			echo menu("../resenas.php", "Reseñas", $seccion);
								echo menu("../recomendaciones.php","Recomendaciones",$seccion);
								echo menu("../noticias.php","Noticias",$seccion);
					  			?>
							</b>
					  	</font>
					</ul>
				</div>
				</div>
				<div id="temas">
					<div id="temas1">
						
					</div>
					<div id="temas2">
						
					</div>
					<div id="temas3">
						<font face="calibri" color="white" size="4" style="text-align: justify">
							<h1>Reseña de: ¡TEKKEN 7!</h1>
							<center>
								<img src="../../../imagenes/tekken7.png" width="500" height="300"/>
							</center>
							<p>
							Tekken, una de las sagas más icónicas de los juegos de pelea, de las pocas que 
							implementaron el movimiento 3D y que sobrevivió para contarlo ya que lo hizo muy 
							bien, tiene su séptima entrega este año por parte Bandai Namco, que cierra con el largo 
							conflicto entre la familia Mishima.
							</p>
							<p>
								Tekken 7 es considerado como el mejor juego de peleas del año, y no es una exageración, 
								ya que todos sus elementos hacen que se gane este título sin ningún problema, debido a 
								sus nuevas mecánicas, historia, personajes, gráficas y más, que tratan de atraer a nuevos 
								jugadores y a aquellos que hayan dejado la saga hace tiempo, aunque si presenta 
								algunas carencias que podrás ver a continuación.
							</p>
							<p>
								En cuanto a su historia podemos ver un gran cambio, ya que en entregas anteriores 
								tenías que terminar el modo historia con cada uno de los personajes e ir entrelazando 
								los distintos finales, ahora la historia es lineal y tendrás que ir pasando cada episodio con 
								el personaje acerca del que gira ese capítulo, pero si extrañas el modo historia anterior, 
								también podrás jugar en episodios únicos de la mayoría de los distintos personajes del 
								juego. Además al comienzo de cada episodio de la historia principal verás como 
								introducción los avances que va consiguiendo un reportero que está investigando este 
								conflicto, ya que su esposa e hijo se vieron afectados por este; aunque es un buen toque 
								el agregar estos intros, llega un momento en el que cansan y hasta fastidian, porque a 
								veces son muy largos.
							</p>
							<center>
								<img src="../../../imagenes/tekken1.jpg" width="400" height="250"/>
							</center>
							<p>
								Sobre el final, no te queremos dar ningún spoiler ni nada por el estilo, solo te diremos 
								que tiene un muy buen cierre la pelea entre padre e hijo, Heihachi  Mishima y Kazuya 
								Mishima, que te dejará completamente satisfecho, aunque puede que te haga 
								experimentar muchas emociones.
							</p>
							<p>
								Ahora, si nunca has jugando algún título de la saga Tekken, pero te interesa mucho este 
								juego, no te preocupes, ya que no tendrás ningún problema al ponerte al corriente con la 
								historia, porque en la galería podrás ver varios breves resúmenes de cada juego que te 
								dan los hechos más importantes que sucedieron en cada uno de ellos. 
							</p>
							<center>
								<img src="../../../imagenes/tekken4.jpg" width="400" height="250"/>
							</center>
							<p>
								Podrás escoger entre varios modos de juego, como el online en el que podrás participar 
								en partidas rankeadas para subir tu rango en línea y en torneos, aunque a veces se tarda 
								mucho en encontrar partidas, o el offline, donde puedes jugar en el modo arcade, donde 
								librarás varias peleas hasta enfrentarte al jefe, el modo combate por tesoros, donde 
								pelearás sin parar mientras vas desbloqueando accesorios para personalizar tu personaje 
								y subirás de rango en el modo offline, el modo práctica, para que mejores tus 
								habilidades, o el modo duelo, donde podrás jugar con tus amigos o con quien esté a tu 
								lado. También están los modos historia y Tekken Bowl, en este último podrás jugar 
								boliche (sí, es en serio, boliche) como en entregas pasadas.
							</p>
							<p>
								En cuanto a las nuevas mecánicas en el sistema de juego, ahora tenemos dos nuevas 
								técnicas, los rage drive y los rage art, que son secuencias de ataques que podemos 
								ejecutar cuando el nivel de la barra de vida de nuestro personaje ya está bajo y está de 
								color rojo, y que son diferentes en cada personaje, además de que con estás técnicas 
								podrás dar la vuelta a la pelea si los sabes ejecutar correctamente en caso de que vayas 
								perdiendo o si vas ganando, podrás dar un final espectacular.
							</p>
							<p>
								En total puedes escoger entre 37 personajes, cada uno con características y estilos 
								distintos (con algunas excepciones), que podrán ayudarte o perjudicarte de acuerdo a tu 
								estilo de juego y como te vayas acomodando con ellos. Podemos ver algunos personajes 
								clásicos conocidos por todos como Jin Kazama, Kazuya Mishima, Heihachi Mishima, 
								Bryan Fury, Nina Williams y más, además de nuevos personajes como Akuma de Street 
								Fighter, Kazumi, Gigas, Lucky Chloe, Shaheen, Katarina, Josie, Claudio y Master Raven. Te 
								recomendamos que pruebes cada personaje y con el que te ajustes más, practiques más 
								tiempo para aprender a dominarlo.
							</p>
							<center>
								<img src="../../../imagenes/tekken2.jpg" width="400" height="200"/>
							</center>
							<p>
								Sobre la personalización de tu personaje podrás encontrar una gran variedad de artículos 
								para arreglarlo como te guste, desde espadas, armas, peinados, playeras, chamarras y 
								hasta bikinis, aunque es un poco decepcionante que le hayan puesto tanto empeño en 
								aspectos como este y no a otras características del juego.
							</p>
							<center>
								<img src="../../../imagenes/tekken3.jpg" width="400" height="200"/>
							</center>
							<p>
								Los gráficos y la música son muy buenos sinceramente, ya que Tekken 7 fue desarrollado 
								en Unreal Engine, pero en algunos escenarios, en el fondo los gráficos por momentos no 
								se ven muy bien o la música llega a ser un poco molesta en otros escenarios, más cuando 
								ya jugaste por un buen rato y si estabas usando audífonos.
							</p>
							<p>
								Para resumir todo esto la historia es muy buena y también el final, aunque no nos agrado 
								tanto los detalles del reportero, los gráficos y la música son buenos pero tienen algunos 
								defectos menores, la jugabilidad es muy buena como de costumbre además de que es un 
								muy buen toque la innovación de los Rage Art y los Rage Drive, la variedad de personajes 
								es muy buena, los modos de juego igual, como el combate por tesoros que es muy 
								adictivo, aunque en el online si desespera lo lento que puede ser la espera para 
								encontrar oponentes y que a veces te enfrenta contra oponentes o de muy alto rango o 
								de un rango muy bajo, y para terminar si se siente que innovó con respecto a los juegos 
								anteriores, y en los aspectos que no, pues, como dice una frase, para que reparar lo que 
								no está roto.
							</p>
							<p>
								Nosotros lo calificaríamos en una escala del 1 al 10 con un 9 ya que a pesar de los pocos 
								defectos que tiene, es muy bueno y muy recomendable.
							</p>
							<p>
								Antes de despedirnos te aclaramos que el análisis fue hecho desde el punto de vista de 
								Os, un fan de la saga que la ha seguido desde Tekken 3 y solo se perdió Tekken Tag 2 y 
								Tekken Revolution, y que sinceramente tenía altas expectativas de este juego y se vieron 
								cumplidas, además de que donde lo jugamos y terminamos fue en PS4, y corroboramos 
								con más personas que al parecer es en donde hay más problemas en el modo online, ya 
								que en Xbox One y en PC por lo que pudimos ver en foros y comentarios no se presenta 
								tanto esto.
							</p>
							<p>
								Recuerda, Tekken 7 es fácil de jugar pero difícil de dominar, además de que deberás 
								aprender a perder para poder ganar.
							</p>
						</font>
							<div id="comments">
								<font face="calibri" color="white" size="4">
								<?php
								echo "<h1>Comentarios</h1>";
								?>
								</font>
								<div id="comments_1">
									<form action="" method="post">
									
									<input type="text" name="publicacion" id="publicacion" value="" placeholder="Realiza tu comentario"/>
									</br>
									</br>
									<input id="boton_publica" type="submit" value="Comentar"/>
								</form>
								
										
										
								<font color="white" face="calibri" size="5">
										
								<?php
								//me conecto a la base de datos
								$conecta = conectaDB();
								 if(conectaDB())
								{
									//traigo todos los suarios
									$rs= mysql_query("SELECT * FROM comentario2 WHERE status=1 ORDER BY idcomentario2 DESC");
								
									if($rs!=NULL)
									{
										
									while ($row=mysql_fetch_array($rs)) 
												{
													$id_post=$row["idusuarios"];
													//nuevo qwery
													
													$usuario_rs= mysql_query("SELECT usuarios FROM usuarios WHERE idusuarios='$id_post' ");
													while ($row_usuario=mysql_fetch_array($usuario_rs)) 
													{
														$usuario=$row_usuario["usuarios"];
													}	
				
													$foto_rs= mysql_query("SELECT foto FROM datos_extra WHERE idusuarios='$id_post'");
													while ($row_foto=mysql_fetch_array($foto_rs))
													{
														$foto=$row_foto["foto"];
														
													}	
													
														echo '<div id="comments_2">';	
														if(isset($foto))
															{
																
																echo '<img src="../../../imagenes_usuarios/'.$foto.'" width="50" height="50" />';
																echo " $usuario :"; 
															}
															else {
																echo '<img src="../../../imagenes/sin_foto.jpg" width="50" height="50" />';
																echo " $usuario :"; 
															}
														echo '<p id="opinion">'.$row["comentarios2"]."</p>";
														
														echo "</div>";
												
												} 
									}
								}
								
									
								?>
								</font>
								</div>
						</div>
					</div>
					<div id="temas4">
						
					</div>
				</div>
			 </div>
			 
			<div id="Pie">
				<div id="pie1">
					
				</div>
				<div id="pie2">
					<div id="pie2_1">
						
					</div>
					<div id="pie2_2">
						<div id="pie2_2_1">
							<font face="arial", color="white"><h3>Búscanos en nuestras redes sociales</h3></font>
						</div>
						<div id="pie2_2_2">
							<font face="arial">
								<img src="../../../imagenes/youtube2.png" align="center"/><a href="https://www.youtube.com/channel/UCUJH5WKAU0J2bAWG3W_ep4g", style="text-align: center"><font color="white">Canal de youtube</font></a> &emsp; &emsp; &emsp; &emsp; &emsp; &emsp;
								<img src="../../../imagenes/facebook.png" align="center"/><a href="https://www.facebook.com/SecretStageYoutubeChannel/", style="text-align: center"><font color="white">Página de Facebook</font></a> &emsp; &emsp; &emsp; &emsp; &emsp; &emsp;
								<img src="../../../imagenes/twitter.png" align="center"/><a href="https://twitter.com/Mat_Cor_Ss", style="text-align: center"><font color="white">Twitter</font></a> &emsp; &emsp; &emsp; &emsp; &emsp; &emsp;
							</font>
						</div>
					</div>
					<div id="pie2_3">
						
					</div>
				</div>
				<div id="pie3">
					
				</div>
			</div>
		</div>
	</body>
</html>