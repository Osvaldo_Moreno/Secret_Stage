<?php

	extract ($_REQUEST);
	//Inicio la sesión 
	session_start();
	//COMPRUEBA QUE EL USUARIO ESTA AUTENTIFICADO 
	if($_SESSION["autentificado"] != "SI") { 
   	//si no existe, envio a la página de autentificacion 
   	header("Location: ../../../login.php"); 
   	//ademas salgo de este script 
	} else {
    //sino, calculamos el tiempo transcurrido
    $fechaGuardada = $_SESSION["ultimoAcceso"];
    $ahora = date("Y-m-d H:i:s");
    $tiempo_transcurrido = (strtotime($ahora)-strtotime($fechaGuardada));

    //comparamos el tiempo transcurrido
     if($tiempo_transcurrido >= 600) {
     //si pasaron 10 minutos o más
      session_destroy(); // destruyo la sesión
      header("Location: ../../../login.php"); //envío al usuario a la pag. de autenticación
      //sino, actualizo la fecha de la sesión
    }else {
    $_SESSION["ultimoAcceso"] = $ahora;
  	 }
	} 
	//incluye funciones
	include '../../../funciones.php';
	$nombre= $_SESSION["nombre"];
	$id_usuario= $_SESSION["id_usuario"];
	//publicacion
	if(isset($publicacion)&&$publicacion!="")
	{
		//me conecto a la base de datos
				$conecta = conectaDB();
				 if(conectaDB())
				{
				
					
					//inserto la publicacion
					$rs= mysql_query("INSERT INTO `comentario8` (`idcomentario8`, `comentarios8`, `status`, `idusuarios`) VALUES (NULL, '$publicacion', '1','$id_usuario')");
			 		if ($rs!=NULL) 
					 {
						//si es diferente de nulo inserto
						
						header("Location: ./recomendacion1.php");  
					}
			 	
				}
	
	}
	
	
?>
<!DOCTYPE> 
<html>
	<head>
		<style type="text/css">
		/*agregamos el fondo */
			body{
				background: url('../../../imagenes/legends.jpg');
				background-size: 100%;
				background-repeat: no-repeat;
				background-attachment: fixed;
				background-color: black;
			}
		</style>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Furi</title>
		<link rel="stylesheet" href="../../../css/estilos.css">
	</head>
	<body>
		<div id="contenedor">
			<div id="cabecera">
				<div id="logo1">
					<br/>
					<><img src="../../../imagenes/logo3.png">
					<br/>
				</div>
				<div id="menu">
					<ul class="horizontal">
						<font face="arial">
			  			<?php
					  		$seccion="Contenido";
					  		echo menu("../../bienvenido.php", "Bienvenido", $seccion);
							echo menu("../../contenido.php","Contenido",$seccion);
							echo menu("../../perfil.php","Perfil",$seccion);
							echo menu("../../foro.php","Foros",$seccion);
							echo menu("../../../index.php","Salir",$seccion);
							
					  	?>
					  	</font>
					</ul>
				</div>
			</div>
			<div >
			 <div id="stage0_0">
				<div  id="stage0_1">
					<div id="menu2">
					<ul class="vertical">
						<font face="arial" size="4">
							<b>
								<?php
					  			$seccion="Recomendaciones";
					  			echo menu("../resenas.php", "Reseñas", $seccion);
								echo menu("../recomendaciones.php","Recomendaciones",$seccion);
								echo menu("../noticias.php","Noticias",$seccion);
					  			?>
							</b>
					  	</font>
					</ul>
				</div>
				</div>
				<div id="temas">
					<div id="temas1">
						
					</div>
					<div id="temas2">
						
					</div>
					<div id="temas3">
						<font face="calibri" color="white" size="4" style="text-align: justify">
							<h1>Te recomendamos: ¡Furi!</h1>
							<center>
								<img src="../../../imagenes/recomendacion.jpg" width="400" height="200"/>
							</center>
							<p>
								Furi es un juego creado por The Game Bakers, del género bullet hell, que como todos los 
								juegos de su género, es EXTREMADAMENTE DIFÍCIL, aunque puede que sea mas fácil si 
								te aprendes los patrones y empleas una buena estrategia, o ya haz jugado algún título de 
								este género.
							</p>
							<p>
								Este juego lo recomendamos a todo los gamers, en especial a los Hardcore Gamers, ya 
								que cuenta con muy buenos elementos que los dejarán fascinados y que 
								mencionaremos a continuación. Además queremos mencionarte que donde lo 
								probamos fue en PS4, en la dificultad Furi (si, como el nombre del juego), que sería la 
								dificultad media y que no contamos con mucha experiencia en este género de juegos, así 
								que también por eso se nos dificultó un poco.
							</p>
							<h2>Mecánicas y sistema de juego</h2>
							<center>
								<img src="../../../imagenes/furi1.jpg" width="400" height="200"/>
							</center>
							<p>
								Su sistema de controles no es muy amplio, ya que solo te permite atacar con la espada, 
								defenderte, hacer disparos débiles pero rápidos, o lanzar disparos lentos pero muy 
								potentes, entre otras cosas, sin embargo, esto no lo limita en nada, ya que es más que 
								suficiente para superar las intensas batallas contra los jefes, aunque es un poco 
								complicado de dominar, ya que debes encontrar el momento adecuado y el ritmo para 
								realizar cada acción, ya que si no lo haces correctamente habrá momentos en los que tu 
								defensa sea completamente inefectiva o fallen tus ataques.
							</p>
							<p>
								Como todo bullet hell, en cada nivel habrá momentos en los que la pantalla esté llena de 
								balas, pero no será así todo el tiempo, Furi cuenta con algo característico, y es que 
								también habrá momentos en los que tendrás que enfrentarte a tu oponente cuerpo a 
								cuerpo utilizando una espada, lo cual le da un toque interesante y más variado, ya que 
								hasta puedes ejecutar impresionantes combos.
							</p>
							<h2>Música</h2>
							<p>
								Cuenta con una muy buena música electrónica de fondo mientras juegas, que concuerda 
								muy bien con los estilos de pelea de los jefes y la situación en la que te encuentras, 
								gracias a esto ya no tendrás que preocuparte por buscar tu playlist favorita en spotify 
								para jugar, ya que disfrutarás completamente la música de algunos artistas como: 
								Waveshaper, Lorn, Scattle, Danger y más. Y por éstas mismas razones, no tendrás ningún 
								problema cuando juegues con tu headset puesto, porque en ningún momento se siente 
								estruendosa o molesta, al contrario, te recomendamos que lo pruebes así y verás que es 
								una experiencia aún mejor.
							</p>
							<h2>Historia</h2>
							<p>
								Aunque no tenga una historia muy impresionante, es buena, es muy interesante conocer 
								el pasado de los enemigos a los que te enfrentas e ir descubriendo que tienen que ver 
								contigo. También los personajes destacan mucho, ya sea por su pasado, habilidades o 
								apariencia, ya que hay algunos muy sobresalientes.
							</p>
							<center>
								<img src="../../../imagenes/furi.jpg" width="400" height="200"/>
							</center>
							<h2>Modos de juego</h2>
							<p>
								Podrás escoger entre 3 modos de juego, el fácil, para todos aquellos que solo quieran 
								divertirse y disfrutar de su historia, ya que a pesar de que su gran característica es su 
								dificultad, sus desarrolladores pensaron en todo y sabían que también habría gamers no 
								muy experimentados que lo quisieran probar; luego está el modo furi (es en el que lo 
								jugamos nosotros), puede interpretarse como la dificultad media, pero que no te deje 
								engañar, a pesar de esto es un gran reto poder terminarlo en esta dificultad, tan solo 
								toma en cuenta que si no terminas el juego en esta dificultad no podrás acceder al modo 
								más complicado de todos, el modo furier, si no te basto con haber terminado el juego en 
								la dificultad anterior, prueba está, para que compruebes tus habilidades.
							</p>
							<h2>¡Es un indie!</h2>
							<p>
								Lo mas impresionante de todo, es que no es un juego AAA, es un indie, a pesar de que 
								fue un equipo pequeño el que lo desarrolló, tiene mejor calidad que algunos juegos de 
								grandes empresas, es un ejemplo de que no hay límites, lo que te propongas, lo puedes 
								lograr.
							</p>
							
						</font>
							<div id="comments">
								<font face="calibri" color="white" size="4">
								<?php
								echo "<h1>Comentarios</h1>";
								?>
								</font>
								<div id="comments_1">
									<form action="" method="post">
									
									<input type="text" name="publicacion" id="publicacion" value="" placeholder="Realiza tu comentario"/>
									</br>
									</br>
									<input id="boton_publica" type="submit" value="Comentar"/>
								</form>
								
										
										
								<font color="white" face="calibri" size="5">
										
								<?php
								//me conecto a la base de datos
								$conecta = conectaDB();
								 if(conectaDB())
								{
									//traigo todos los suarios
									$rs= mysql_query("SELECT * FROM comentario8 WHERE status=1 ORDER BY idcomentario8 DESC");
								
									if($rs!=NULL)
									{
										
									while ($row=mysql_fetch_array($rs)) 
												{
													$id_post=$row["idusuarios"];
													//nuevo qwery
													
													$usuario_rs= mysql_query("SELECT usuarios FROM usuarios WHERE idusuarios='$id_post' ");
													while ($row_usuario=mysql_fetch_array($usuario_rs)) 
													{
														$usuario=$row_usuario["usuarios"];
													}	
				
													$foto_rs= mysql_query("SELECT foto FROM datos_extra WHERE idusuarios='$id_post'");
													while ($row_foto=mysql_fetch_array($foto_rs))
													{
														$foto=$row_foto["foto"];
														
													}	
													
														echo '<div id="comments_2">';	
														if(isset($foto))
															{
																
																echo '<img src="../../../imagenes_usuarios/'.$foto.'" width="50" height="50" />';
																echo " $usuario :"; 
															}
															else {
																echo '<img src="../../../imagenes/sin_foto.jpg" width="50" height="50" />';
																echo " $usuario :"; 
															}
														echo '<p id="opinion">'.$row["comentarios8"]."</p>";
														
														echo "</div>";
												
												} 
									}
								}
								
									
								?>
								</font>
								</div>
						</div>
					</div>
					<div id="temas4">
						
					</div>
				</div>
			 </div>
			 
			<div id="Pie">
				<div id="pie1">
					
				</div>
				<div id="pie2">
					<div id="pie2_1">
						
					</div>
					<div id="pie2_2">
						<div id="pie2_2_1">
							<font face="arial", color="white"><h3>Búscanos en nuestras redes sociales</h3></font>
						</div>
						<div id="pie2_2_2">
							<font face="arial">
								<img src="../../../imagenes/youtube2.png" align="center"/><a href="https://www.youtube.com/channel/UCUJH5WKAU0J2bAWG3W_ep4g", style="text-align: center"><font color="white">Canal de youtube</font></a> &emsp; &emsp; &emsp; &emsp; &emsp; &emsp;
								<img src="../../../imagenes/facebook.png" align="center"/><a href="https://www.facebook.com/SecretStageYoutubeChannel/", style="text-align: center"><font color="white">Página de Facebook</font></a> &emsp; &emsp; &emsp; &emsp; &emsp; &emsp;
								<img src="../../../imagenes/twitter.png" align="center"/><a href="https://twitter.com/Mat_Cor_Ss", style="text-align: center"><font color="white">Twitter</font></a> &emsp; &emsp; &emsp; &emsp; &emsp; &emsp;
							</font>
						</div>
					</div>
					<div id="pie2_3">
						
					</div>
				</div>
				<div id="pie3">
					
				</div>
			</div>
		</div>
	</body>
</html>