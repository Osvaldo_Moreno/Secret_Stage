<?php
extract ($_REQUEST);
include './funciones.php';
?>
<!DOCTYPE> 

<html>
	<head>
		<style type="text/css">
		/*agregamos el fondo */
			body{
				background: url('./imagenes/legends.jpg');
				background-size: 100%;
				background-repeat: no-repeat;
				background-attachment: fixed;
				background-color: black;
			}
		</style>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>¿Qué es Secret Stage?</title>
		<link rel="stylesheet" href="./css/estilos.css">
		<script src="./js/slider.js"></script>
	</head>
	<body>
		<div id="contenedor">
			<div id="cabecera">
				<div id="logo1">
					<br/>
					<><img src="./imagenes/logo3.png">
					<br/>
				</div>
				<div id="menu">
					<ul class="horizontal">
						<font face="arial">
			  			<?php
					  		$seccion="¿Qué es Secret Stage?";
					  		echo menu("./index.php", "Inicio", $seccion);
							echo menu("./que_es.php","¿Qué es Secret Stage?",$seccion);
							echo menu("./login.php","Inicia sesión",$seccion);
							echo menu("./registrate.php","Regístrate",$seccion);
					  	?>
					    </font>
					</ul>
				</div>
			</div>
			<div id="contenido3">
				<div id="c-slider" class="w3-content w3-display-container">
					<div id="slider">
						<section>
						<img class="mySlides" src="./imagenes/slider1.jpg" style="width:100%">
						</section>
						<section>
						<img class="mySlides" src="./imagenes/slider2.jpg" style="width:100%">
						</section>
						<section>
						<img class="mySlides" src="./imagenes/slider6.jpg" style="width:100%">
						</section>
						<section>
						<img class="mySlides" src="./imagenes/slider5.jpg" style="width:100%">
						</section>

					</div>
					<div id="btn-prev">
					<button style="background: rgba(255, 255, 255, 0.7); border-radius: 100%; width: 40px; height: 40px; font-size: 20px; font-weight: bold; text-align: center" onclick="plusDivs(-1)">&#10094;</button>
					</div>
					<div id="btn-next">
					<button style="background: rgba(255, 255, 255, 0.7); border-radius: 100%; width: 40px; height: 40px; font-size: 20px; font-weight: bold; text-align: center" onclick="plusDivs(1)">&#10095;</button>
					</div>
				</div>
			</div>
			<div id="contenido4">
				<div id="contenido4_1">
					
				</div>
				<div id="contenido4_2">
					
				</div>
				<div id="contenido4_3">
					<font face="calibri", color="white", style="text-align: justify">
						<font size="5"><h1>¿Qué es Secret Stage?</h1></font>
						<font size="5">
							<p>
							Secret Stage es un sitio donde encontrarás todo lo que quieras saber acerca del mundo de los videojuegos, anime, cómics y mucho más, además de que es un sitio para todos,
							ya que no importa que tantos conocimientos tengas acerca de alguno de estos temas, ya que encontrarás información de una forma muy especializada o 
							de una forma que todos puedan entender intentando explicar cualquier termino complicado.
							</br>
							</br>
							Aquí podrás encontrar:
							</br>
							</br>
							-Reseñas
							</br>
							-Recomendaciones
							</br>
							-Opiniones
							</br>
							-Noticias
							</br>
							-Tops
							</br>
							-Temas random
							</br>
							Entre otras cosas.
							</br>
							</br>
							También podrás acceder a la sección de foros, donde podrás aclarar las dudas que tengas
							acerca de algun videojuego, pedir consejos para pasar algun nivel o simplemente 
							compartir tu opinión con los demás, además de que podrás conocer a muchas gente que 
							comparta tus mismos gustos e intereses.
							</br>
							</br>
							Para poder acceder a todo esto solo tendrás que crear una cuenta en la sección 
							"Registrate", donde pondrás tu nombre real, escogerás tu nombre de usuario y tu 
							contraseña, al finalizar esto ya sólo tendrás que iniciar sesión y ¡Listo!, podrás revisar todo 
							el contenido y participar en los foros.
							</br>
							</br>
							En las imágenes que se encuentran en la parte superior podrás ver un poco de lo que 
							encontrarás al crear tu cuenta e ingresar, además de que si quieres saber como es el 
							contenido del sitio aquí te dejamos un adelanto:
							</br>
							<h2>Reseña de Anime ¡GAMERS!</h2>
							Gamers es un anime del género de romance, con una buena dosis de comedia, gracias a 
							una serie de confusiones que se dan a lo largo de la historia.
							</br>
							</br>
							La historia gira alrededor de un joven estudiante llamado Amano, que se la pasa todo el 
							tiempo jugando videojuegos, como todo buen gamer y como lo hacemos todos nosotros, 
							además de sus amigos Karen Tendou, Chiaki Hoshinomori, Tasuku y Aguri, que irá 
							conociendo con el paso del tiempo.
							</br>
							</br>
							El anime comienza cuando...
							<h2>Reseña de TEKKEN 7</h2>
							Tekken 7 es considerado como el mejor juego de peleas del año, y no es una exageración, 
							ya que todos sus elementos hacen que se gane este título sin ningún problema, debido a 
							sus nuevas mecánicas, historia, personajes, gráficas y más, que tratan de atraer a nuevos 
							jugadores y a aquellos que hayan dejado la saga hace tiempo, aunque si presenta algunas 
							carencias que podrás ver a continuación.
							</br>
							</br>
							En cuanto a su historia podemos ver un gran cambio, ya que...
							<h2>Si te gusto lo que has podido ver, ¿Qué estás esperando? ¡Registrate ya! y no te pierdas ninguno de nuestros contenidos.</h2>
							Además también puedes seguirnos en nuestras redes sociales que aparecen en el pie de página.
						</p>
						</font>
					</font>
				</div>
				<div id="contenido4_4">
					
				</div>
			</div>
			<div id="Pie">
				<div id="pie1">
					
				</div>
				<div id="pie2">
					<div id="pie2_1">
						
					</div>
					<div id="pie2_2">
						<div id="pie2_2_1">
							<font face="arial", color="white"><h3>Búscanos en nuestras redes sociales</h3></font>
						</div>
						<div id="pie2_2_2">
							<font face="arial">
								<img src="./imagenes/youtube2.png" align="center"/><a href="https://www.youtube.com/channel/UCUJH5WKAU0J2bAWG3W_ep4g", style="text-align: center"><font color="white">Canal de youtube</font></a> &emsp; &emsp; &emsp; &emsp; &emsp; &emsp;
								<img src="./imagenes/facebook.png" align="center"/><a href="https://www.facebook.com/SecretStageYoutubeChannel/", style="text-align: center"><font color="white">Página de Facebook</font></a> &emsp; &emsp; &emsp; &emsp; &emsp; &emsp;
								<img src="./imagenes/twitter.png" align="center"/><a href="https://twitter.com/Mat_Cor_Ss", style="text-align: center"><font color="white">Twitter</font></a> &emsp; &emsp; &emsp; &emsp; &emsp; &emsp;
							</font>
						</div>
					</div>
					<div id="pie2_3">
						
					</div>
				</div>
				<div id="pie3">
					
				</div>
			</div>
		</div>
	</body>
</html>